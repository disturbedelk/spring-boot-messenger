package ru.tsu.hits.messenger.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.hits.messenger.common.data.Pagination;
import ru.tsu.hits.messenger.entity.NotificationType;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetUserNotificationsRequestBody {
    private Pagination pagination;
    private Date from;
    private Date to;
    private String wildcard;
    private List<NotificationType> types;
}
