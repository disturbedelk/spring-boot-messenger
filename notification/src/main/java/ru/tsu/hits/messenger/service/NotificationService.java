package ru.tsu.hits.messenger.service;

import lombok.RequiredArgsConstructor;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.tsu.hits.messenger.common.data.PagedView;
import ru.tsu.hits.messenger.common.data.Pagination;
import ru.tsu.hits.messenger.dto.GetUserNotificationsRequestBody;
import ru.tsu.hits.messenger.dto.NotificationDto;
import ru.tsu.hits.messenger.dto.SetNotificationsRequestBody;
import ru.tsu.hits.messenger.entity.Notification;
import ru.tsu.hits.messenger.entity.NotificationStatus;
import ru.tsu.hits.messenger.entity.NotificationType;
import ru.tsu.hits.messenger.repository.NotificationRepository;
import ru.tsu.hits.messenger.utility.NotificationDtoEntityConverter;

import java.util.Date;
import java.util.UUID;
import java.util.stream.Collectors;

import static ru.tsu.hits.messenger.entity.NotificationStatus.*;

@Service
@RequiredArgsConstructor
public class NotificationService {
    private final NotificationRepository notificationRepository;


    public PagedView<NotificationDto> getUserNotifications(GetUserNotificationsRequestBody request, String userId) {
        var from = request.getFrom();
        var to = request.getTo();
        if(from != null && to != null && from.after(to)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid dates");
        }
        Sort sort = Sort.by(Sort.Direction.DESC, "received");

        var p = request.getPagination();
        Pageable pageable = PageRequest.of(p.getPage(), p.getSize(), sort);

        var result = notificationRepository.findAllUserNotifications(userId,
                from,
                to,
                request.getWildcard(),
                pageable
        );

        return new PagedView<NotificationDto>(
                result.getContent()
                        .stream()
                        .map(NotificationDtoEntityConverter::notificationDtoFromEntity)
                        .collect(Collectors.toList()),
                Pagination.fromPageable(result.getPageable())
        );
    }

    public long getUserUnreadNotifications(String userId){
        return notificationRepository.getUserNotificationCount(userId, NotificationStatus.PENDING);
    }

    public long setUserNotifications(SetNotificationsRequestBody requestBody, String userId) {
        var status = requestBody.getStatus();
        if(status != DISCARDED && status != RECIEVED && status != READ){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        var notifications = notificationRepository.findAllByTargetUserId(userId)
                .stream()
                .filter(x -> requestBody.getNotificationIds().contains(x.getId()))
                .collect(Collectors.toList());
        for (var notification: notifications) {
            notification.setStatus(requestBody.getStatus());
            if(requestBody.getStatus() == RECIEVED){
                notification.setReceived(new Date());
            }
            if(requestBody.getStatus() == READ) {
                notification.setRead(new Date());
            }
            if(requestBody.getStatus() == DISCARDED){
                notification.setRead(null);
                notification.setReceived(null);
            }
        }
        notificationRepository.saveAll(notifications);
        return notificationRepository.getUserNotificationCount(userId, PENDING);
    }

    public void loginCompleteNotificationEvent(String userId) {
        var notification = new Notification(UUID.randomUUID().toString(),
                NotificationType.LOGIN_COMPLETE,
                "There has been a successful login attempt for your account",
                userId,
                PENDING,
                null,
                new Date()
        );

        notificationRepository.save(notification);
    }
}
