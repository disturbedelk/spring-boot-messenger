package ru.tsu.hits.messenger.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.hits.messenger.entity.NotificationStatus;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SetNotificationsRequestBody {
    private List<String> notificationIds;
    private NotificationStatus status;
}
