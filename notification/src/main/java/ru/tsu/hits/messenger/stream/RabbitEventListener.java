package ru.tsu.hits.messenger.stream;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.tsu.hits.messenger.service.NotificationService;
import ru.tsu.hits.messenger.common.dto.stream.InterUserEventDto;

import java.util.function.Consumer;

@Slf4j
@Configuration
public class RabbitEventListener {
    @Autowired
    private NotificationService notificationService;
    @Bean
    public Consumer<String> userLoginCompleted(){
        return userId -> {
            log.info(String.format("User {id:%s} has successfuly completed login", userId));
            notificationService.loginCompleteNotificationEvent(userId);
        };
    }
    @Bean
    public Consumer<InterUserEventDto> userPersonalMessageReceived(){
        return dto -> {
            log.info("User {id:" + dto.getUserId() + "} sent a message to {id:" + dto.getTargetUserId() + "}");
            notificationService.userPmReceivedEvent(dto);
        }
    }

    public Consumer
}
