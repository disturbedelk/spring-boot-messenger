package ru.tsu.hits.messenger.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "notifications")
@AllArgsConstructor
@NoArgsConstructor
public class Notification {
    @Id
    private String id;
    @Enumerated(EnumType.STRING)
    @NonNull
    private NotificationType type;
    private String text;
    @NonNull
    @Column(name = "target_user_id")
    private String targetUserId;
    @Enumerated(EnumType.STRING)
    @NonNull
    private NotificationStatus status;
    private Date read;
    private Date received;
}
