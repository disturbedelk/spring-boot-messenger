package ru.tsu.hits.messenger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import ru.tsu.hits.messenger.common.security.EnableSecurityConfiguration;
import ru.tsu.hits.messenger.common.security.props.EnableSecurityProps;

@SpringBootApplication
@ConfigurationPropertiesScan("ru.tsu.hits.messenger.common.security")
@EnableSecurityProps
@EnableSecurityConfiguration
public class NotificationServer {
    public static void main(String[] args) {
        SpringApplication.run(NotificationServer.class, args);
    }
}