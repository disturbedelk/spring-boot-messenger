package ru.tsu.hits.messenger.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tsu.hits.messenger.common.data.PagedView;
import ru.tsu.hits.messenger.common.security.data.JwtUserData;
import ru.tsu.hits.messenger.dto.GetUserNotificationsRequestBody;
import ru.tsu.hits.messenger.dto.NotificationDto;
import ru.tsu.hits.messenger.dto.SetNotificationsRequestBody;
import ru.tsu.hits.messenger.service.NotificationService;

@Slf4j
@RestController
@RequestMapping("/notifications")
@RequiredArgsConstructor
public class NotificationController {
    private final NotificationService notificationService;

    @GetMapping
    private PagedView<NotificationDto> getUserNotifications(GetUserNotificationsRequestBody body, Authentication authentication){
        var userId = ((JwtUserData)authentication.getPrincipal()).getId();
        return notificationService.getUserNotifications(body, userId.toString());
    }

    @GetMapping("/unread")
    private long getUserUnread(Authentication authentication){
        var userId = ((JwtUserData)authentication.getPrincipal()).getId();
        return notificationService.getUserUnreadNotifications(userId.toString());
    }

    @PostMapping("/set-state")
    private long setUserNotifications(SetNotificationsRequestBody body, Authentication authentication){
        var userId = ((JwtUserData)authentication.getPrincipal()).getId();
        return notificationService.setUserNotifications(body, userId.toString());
    }
}
