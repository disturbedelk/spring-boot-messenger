package ru.tsu.hits.messenger.entity;

public enum NotificationType {
    FRIEND_REQUEST,
    FRIEND_REMOVED,
    MESSAGE_RECEIVED,
    LOGIN_COMPLETE
}
