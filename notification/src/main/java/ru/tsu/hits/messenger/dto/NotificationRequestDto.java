package ru.tsu.hits.messenger.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.tsu.hits.messenger.entity.NotificationType;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationRequestDto {
    @NonNull
    private String userId;
    private NotificationType type;
    private String text;
}
