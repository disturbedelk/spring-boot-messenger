package ru.tsu.hits.messenger.entity;

public enum NotificationStatus {
    PENDING,
    RECIEVED,
    READ,
    DISCARDED
}
