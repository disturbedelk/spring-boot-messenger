package ru.tsu.hits.messenger.repository;

import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsu.hits.messenger.entity.NotificationStatus;
import ru.tsu.hits.messenger.entity.NotificationType;
import ru.tsu.hits.messenger.entity.Notification;
import ru.tsu.hits.messenger.entity.Notification_;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface NotificationRepository extends JpaRepository<Notification, String>, JpaSpecificationExecutor<Notification> {
    default Page<Notification> findAllUserNotifications(String targetUserId, Date from, Date to, String wildcard, Pageable pageable){
        Specification<Notification> specification = (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.equal(root.get(Notification_.targetUserId), targetUserId));

            if(from != null){
                predicates.add(cb.greaterThanOrEqualTo(root.<Date>get(Notification_.received), from));
            }

            if(to != null){
                predicates.add(cb.lessThanOrEqualTo(root.<Date>get(Notification_.received), to));
            }

            if(wildcard != null && !wildcard.isBlank()){
                predicates.add(cb.like(root.get(Notification_.text), "%" + wildcard + "%"));
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };

        return findAll(specification, pageable);
    }

    @Query("select count(n) from Notification n where n.targetUserId = :userId and n.status = :notificationStatus")
    long getUserNotificationCount(@Param("userId") String userId, @Param("notificationStatus") NotificationStatus status);

    List<Notification> findAllByTargetUserId(String targetUserId);
}
