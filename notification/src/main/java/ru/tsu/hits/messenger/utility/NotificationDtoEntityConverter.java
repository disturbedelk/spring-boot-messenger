package ru.tsu.hits.messenger.utility;

import lombok.experimental.UtilityClass;
import ru.tsu.hits.messenger.dto.NotificationDto;
import ru.tsu.hits.messenger.entity.Notification;

@UtilityClass
public class NotificationDtoEntityConverter {
    public NotificationDto notificationDtoFromEntity(Notification notification){
        return new NotificationDto(notification.getId(),
                notification.getType(),
                notification.getText(),
                notification.getStatus(),
                notification.getReceived());
    }
}
