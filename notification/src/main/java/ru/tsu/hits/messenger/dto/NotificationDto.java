package ru.tsu.hits.messenger.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.hits.messenger.entity.NotificationType;
import ru.tsu.hits.messenger.entity.NotificationStatus;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationDto {
    private String id;
    private NotificationType type;
    private String text;
    private NotificationStatus status;
    private Date recieved;
}
