package ru.tsu.hits.messenger.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
//import org.springframework.cloud.gateway.route.RouteLocator;
//import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.cloud.gateway.filter.factory.ThrottleGatewayFilterFactory;
//
//
//import java.util.concurrent.TimeUnit;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class GatewayServer {
    public static void main(String[] args) {
        SpringApplication.run(GatewayServer.class);
    }
//TODO: CUSTOM ROUTE LOCATOR NOT FUNCTIONAL, ThrottleGatewayFilterFactory COULD NOT BE IMPORTED FROM spring-cloud-gateway
// ALSO: Unresolvable class definition for class [org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JConfigurationProperties]

//    @Bean
//    public RouteLocator customRouteLocator(RouteLocatorBuilder builder, ThrottleGatewayFilterFactory throttle) {
//        return builder.routes()
//                .route(r -> r.host("**.abc.org").and().path("/image/png")
//                        .filters(f ->
//                                f.addResponseHeader("X-TestHeader", "foobar"))
//                        .uri("http://httpbin.org:80")
//                )
//                .route(r -> r.path("/image/webp")
//                        .filters(f ->
//                                f.addResponseHeader("X-AnotherHeader", "baz"))
//                        .uri("http://httpbin.org:80")
//                        .metadata("key", "value")
//                )
//                .route(r -> r.order(-1)
//                        .host("**.throttle.org").and().path("/get")
//                        .filters(f -> f.filter(throttle.apply(1,
//                                1,
//                                10,
//                                TimeUnit.SECONDS)))
//                        .uri("http://httpbin.org:80")
//                        .metadata("key", "value")
//                )
//                .build();
//    }
//    @Bean
//    public RouteLocator myRoutes(RouteLocatorBuilder builder) {
//        return builder.routes()
//            .route(p -> p
//                .path("/users")
//                .uri("http://localhost:8103/users"))
//            .route(p -> p
//                .path("/users/**")
//                .uri("http://localhost:8103/users/"))
//            .route(p -> p
//                .path("/friends")
//                .uri("http://localhost:8104/friends"))
//            .route(p -> p
//                .path("/friends/**")
//                .uri("http://localhost:8104/friends/"))
//            .build();
//    }
}