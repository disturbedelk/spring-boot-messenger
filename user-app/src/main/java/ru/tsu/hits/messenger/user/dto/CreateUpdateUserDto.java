package ru.tsu.hits.messenger.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.hits.messenger.user.entity.Gender;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUpdateUserDto {

    @NotBlank(message = "User Login is mandatory")
    private String login;
    @NotBlank(message = "Password is mandatory")
    private String password;
    @Email
    private String email;
    @NotBlank(message = "Full Name is mandatory")
    private String fullName;
    private Date dateOfBirth;
    private String city;
    private String avatar;
    private String phoneNumber;
    private Gender gender;
}
