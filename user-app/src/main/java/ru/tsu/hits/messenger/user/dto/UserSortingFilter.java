package ru.tsu.hits.messenger.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.hits.messenger.user.utility.SortingOrder;
import ru.tsu.hits.messenger.user.utility.UsersSortingField;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserSortingFilter {
    private UsersSortingField field;
    private SortingOrder order;
}
