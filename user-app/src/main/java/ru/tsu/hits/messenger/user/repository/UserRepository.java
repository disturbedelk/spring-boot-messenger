package ru.tsu.hits.messenger.user.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ru.tsu.hits.messenger.user.entity.UserEntity;

import java.util.Date;
import java.util.List;

public interface UserRepository extends JpaRepository<UserEntity, String>, JpaSpecificationExecutor<UserEntity> {
    @Query("SELECT NEW ru.tsu.hits.messenger.user.entity.UserEntity(e.id, e.fullName) FROM UserEntity e WHERE e.id = ?1")
    List<UserEntity> getUserFullNameById(String id);
    @Query(value = "SELECT e FROM UserEntity e WHERE e.login = ?1")
    UserEntity getUserByLogin(String login);
    boolean existsUserByLogin(String login);
    boolean existsUserByEmail(String email);
    //TODO: ADD GENDER TO QUERY
    @Query(value = "SELECT * FROM users e WHERE " +
            "(:login IS NULL OR LOWER(e.login) LIKE LOWER(CONCAT('%', COALESCE(:login, ''), '%')))" +
            "AND (:email IS NULL OR e.email LIKE CONCAT('%', COALESCE(:email, ''), '%'))" +
            "AND (:fullName IS NULL OR LOWER(e.full_name) LIKE LOWER(CONCAT('%', COALESCE(:fullName, ''), '%')))" +
            "AND (:phoneNumber IS NULL OR e.phone_number LIKE CONCAT('%', COALESCE(:phoneNumber, ''), '%'))" +
            "AND (:city IS NULL OR e.city LIKE CONCAT('%', COALESCE(:city, ''), '%'))" +
            "AND (CAST(:dateOfBirth AS date) IS NULL OR " +
            "        CASE " +
            "            WHEN :dateOfBirthComparator = 0 THEN e.date_of_birth < CAST(:dateOfBirth AS date) " +
            "            WHEN :dateOfBirthComparator = 1 THEN e.date_of_birth = CAST(:dateOfBirth AS date) " +
            "            WHEN :dateOfBirthComparator = 2 THEN e.date_of_birth > CAST(:dateOfBirth AS date) " +
            "            ELSE FALSE " +
            "        END" +
            "    ) " +
//            "ORDER BY " +
//            "   CASE WHEN :full_name_order <> 'NONE' THEN " +
//            "      CASE WHEN :full_name_order = 'ASC' THEN full_name END ASC," +
//            "      CASE WHEN :full_name_order = 'DESC' THEN full_name END DESC," +
//            "      full_name" +
//            "   END" +
//            "   CASE WHEN :login_order <> 'NONE' THEN " +
//            "      CASE WHEN :login_order = 'ASC' THEN login END ASC," +
//            "      CASE WHEN :login_order = 'DESC' THEN login END DESC," +
//            "      login" +
//            "   END," +
//            "   CASE WHEN :email_order <> 'NONE' THEN " +
//            "      CASE WHEN :email_order = 'ASC' THEN email END ASC," +
//            "      CASE WHEN :email_order = 'DESC' THEN email END DESC," +
//            "      email" +
//            "   END," +
//            "   CASE WHEN :date_of_birth_order <> 'NONE' THEN " +
//            "      CASE WHEN :date_of_birth_order = 'ASC' THEN date_of_birth END ASC," +
//            "      CASE WHEN :date_of_birth_order = 'DESC' THEN date_of_birth END DESC," +
//            "      date_of_birth" +
//            "   END," +
//            "   CASE WHEN :city_order <> 'NONE' THEN " +
//            "      CASE WHEN :city_order = 'ASC' THEN city END ASC," +
//            "      CASE WHEN :city_order = 'DESC' THEN city END DESC," +
//            "      city" +
//            "   END," +
//            "   CASE WHEN :phone_number_order <> 'NONE' THEN " +
//            "      CASE WHEN :phone_number_order = 'ASC' THEN phone_number END ASC," +
//            "      CASE WHEN :phone_number_order = 'DESC' THEN phone_number END DESC," +
//            "      phone_number" +
//            "   END," +
//            "   CASE WHEN :gender_order <> 'NONE' THEN " +
//            "      CASE WHEN :gender_order = 'ASC' THEN gender END ASC," +
//            "      CASE WHEN :gender_order = 'DESC' THEN gender END DESC," +
//            "      gender" +
//            "   END" +
            "LIMIT :size OFFSET :offset",
    nativeQuery = true)
    List<UserEntity> findUsersRequest(
        @Param("login") String login,
        @Param("email") String email,
        @Param("fullName") String fullName,
        @Param("dateOfBirth") Date dateOfBirth,
        @Param("dateOfBirthComparator") int dobComparator,
        @Param("phoneNumber") String phoneNumber,
        @Param("city") String city,
        @Param("size") int pageSize,
        @Param("offset") int offset
//        @Param("full_name_order") String fullNameOrder,
//        @Param("login_order") String loginOrder,
//        @Param("email_order") String emailOrder,
//        @Param("date_of_birth_order") String dobOrder,
//        @Param("city_order") String cityOrder,
//        @Param("phone_number_order") String phoneNumberOrder,
//        @Param("gender_order") String genderOrder
    );
    List<UserEntity> findAll(Specification<UserEntity> spec);
    Page<UserEntity> findAll(Specification<UserEntity> spec, Pageable pageable);
}
