package ru.tsu.hits.messenger.user.entity;

public enum Gender {
    MALE,
    FEMALE,
    BACKEND_DEVELOPER
}
