package ru.tsu.hits.messenger.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.lang.Nullable;
import ru.tsu.hits.messenger.common.data.Pagination;
import ru.tsu.hits.messenger.user.entity.Gender;
import ru.tsu.hits.messenger.user.utility.DateComparisonType;

import javax.validation.constraints.Email;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUsersRequestDto {
    @Nullable
    private String login;
    @Email
    @Nullable
    private String email;
    @Nullable
    private String fullName;
    @Nullable
    private Date dateOfBirth;
    @Nullable
    private DateComparisonType dateOfBirthComparator;
    @Nullable
    private String phoneNumber;
    @Nullable
    private String city;
    @Nullable
    private Gender gender;
    @NonNull
    private Pagination pagination;
    private List<UserSortingFilter> orders;
}
