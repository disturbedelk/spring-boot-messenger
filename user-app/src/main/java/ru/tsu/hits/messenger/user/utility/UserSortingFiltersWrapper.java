package ru.tsu.hits.messenger.user.utility;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ru.tsu.hits.messenger.user.dto.UserSortingFilter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class UserSortingFiltersWrapper {
    public SortingOrder getOrder(UsersSortingField field){
        if(orders == null || orders.size() == 0){
            return SortingOrder.NONE;
        }
        var found = orders.stream().filter(val -> val.getField() == field).findFirst();
        return found.isPresent() ? found.get().getOrder() : SortingOrder.NONE;
    }
    private List<UserSortingFilter> orders;
}
