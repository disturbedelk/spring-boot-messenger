package ru.tsu.hits.messenger.user.utility;

public enum UsersSortingField {
    FULL_NAME,
    LOGIN,
    EMAIL,
    BIRTHDAY,
    CITY
}
