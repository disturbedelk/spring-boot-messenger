package ru.tsu.hits.messenger.user.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tsu.hits.messenger.user.dto.UserDto;
import ru.tsu.hits.messenger.user.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/integration/users")
@RequiredArgsConstructor
public class UserIntegrationController {
    private final UserService userService;

    @GetMapping(value = "/{id}/exists", produces = "application/json")
    public UserDto getUserById(@PathVariable String id){
        return userService.getUserFullNameById(id);
    }

    @GetMapping(value = "/exist", produces = "application/json")
    public List<UserDto> getUsers(@RequestBody List<String> userIds){
        return userService.getUsers(userIds);
    }
}
