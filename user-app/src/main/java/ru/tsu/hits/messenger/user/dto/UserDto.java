package ru.tsu.hits.messenger.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.hits.messenger.user.entity.Gender;
import ru.tsu.hits.messenger.user.entity.UserEntity;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    @NotBlank
    private String uuid;
    private String fullName;
    private String login;
    private String email;
    private String phoneNumber;
    private Date dateOfBirth;
    private Date registered;
    private String city;
    private String avatar;
    private Gender gender;
}
