package ru.tsu.hits.messenger.user.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity {
    @Id
    @Column(name = "id")
    @NonNull
    private String uuid;

    @Column(unique = true)
    @NonNull
    private String login;

    @Column(unique = true)
    private String email;

    @Column
    @NonNull
    private String password;

    @Column(name = "full_name")
    @NonNull
    private String fullName;

    @Column(name = "date_of_birth")
    @Temporal(TemporalType.DATE)
    @NonNull
    private Date dateOfBirth;

    @Column
    @Temporal(TemporalType.DATE)
    @NonNull
    private Date registered;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column
    private String city;

    @Column(name = "avatar_uuid")
    private String avatarUUID;

    @Column
    @Enumerated(EnumType.STRING)
    @NonNull
    private Gender gender;
    public UserEntity(String id, String fullName){
        uuid = id;
        this.fullName = fullName;
    }
}
