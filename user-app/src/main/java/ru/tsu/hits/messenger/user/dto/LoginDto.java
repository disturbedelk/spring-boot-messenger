package ru.tsu.hits.messenger.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginDto {
    @NotBlank(message = "User login is mandatory")
    private String login;
    @NotBlank(message = "User password is mandatory")
    private String password;
}
