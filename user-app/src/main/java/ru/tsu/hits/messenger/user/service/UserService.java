package ru.tsu.hits.messenger.user.service;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import ru.tsu.hits.messenger.common.data.PagedView;
import ru.tsu.hits.messenger.common.data.Pagination;
import ru.tsu.hits.messenger.common.security.data.SecurityConst;
import ru.tsu.hits.messenger.common.utility.InterServiceRequestHandler;
import ru.tsu.hits.messenger.user.dto.CreateUpdateUserDto;
import ru.tsu.hits.messenger.user.dto.GetUsersRequestDto;
import ru.tsu.hits.messenger.user.dto.LoginDto;
import ru.tsu.hits.messenger.user.dto.UserDto;
import ru.tsu.hits.messenger.user.entity.UserEntity;
import ru.tsu.hits.messenger.user.entity.UserEntityConverter;
import ru.tsu.hits.messenger.user.repository.UserRepository;
import ru.tsu.hits.messenger.user.repository.UserSpecifications;
import ru.tsu.hits.messenger.user.utility.*;
import ru.tsu.hits.messenger.common.data.DefaultPaginationConfig;
import org.springframework.cloud.stream.function.StreamBridge;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final AuthenticationService authenticationService;
    private final Logger logger = LoggerFactory.getLogger(UserService.class);
    private final DefaultPaginationConfig paginationDefault;
    private final InterServiceRequestHandler requestHandler;
    private final StreamBridge streamBridge;

    @Transactional
    public ResponseEntity<UserDto> create(CreateUpdateUserDto createUpdateUserDto){
        if(userRepository.existsUserByLogin(createUpdateUserDto.getLogin())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with login already exists");
        }
        if(userRepository.existsUserByEmail(createUpdateUserDto.getEmail())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with email already exists");
        }

        var userEntity = UserEntityConverter.userFromCreateUpdateDto(createUpdateUserDto);
        userEntity.setPassword(authenticationService.encode(createUpdateUserDto.getPassword()));
        userEntity.setUuid(String.valueOf(UUID.randomUUID()));
        userEntity.setRegistered(new Date());// Literally not a better way to do in Java...
        var user = userRepository.save(userEntity);

        var userDto = UserEntityConverter.userDtoFromEntity(user);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date formattedDate;
        try {
            formattedDate = formatter.parse(formatter.format(user.getDateOfBirth()));
        } catch (ParseException e) {
            formattedDate = user.getDateOfBirth();
            logger.error("Failed to format date");
        }
        userDto.setDateOfBirth(formattedDate);
        return login(new LoginDto(userDto.getLogin(), createUpdateUserDto.getPassword()));
    }

    public UserDto readAuthorized(String requestingUserUUID, String login){
        var user = userRepository.getUserByLogin(login);
        if(user == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

//        if(user.getUuid().equals(requestingUserUUID)){
//            return UserEntityConverter.userDtoFromEntity(user);
//        }

        var response = requestHandler.integrationRequest(
                "friends",
                HttpMethod.GET,
                String.class,
                String.format("/blacklist/exists?targetUserId=%s&userId=%s", user.getUuid(), requestingUserUUID),
                ""
        );

        if(response.getStatusCode() == HttpStatus.OK && response.getBody().toString().contains("true")){
            logger.warn(response.getBody().toString());
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You are blacklisted by this user");
        }
        if(response.getStatusCode() != HttpStatus.OK){
            logger.warn(String.format("UserService failed to retrieve blacklist data for %s... is friends service down?", user.getUuid()));
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to retrieve user data");
        }
        return UserEntityConverter.userDtoFromEntity(user);
    }

    public UserDto read(String login){
        var user = userRepository.getUserByLogin(login);
        if(user == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User with login %s does not exist", login));
        }

        return UserEntityConverter.userDtoFromEntity(user);
    }

    @Transactional
    public UserDto update(String login, CreateUpdateUserDto createUpdateUserDto){
        var user = userRepository.getUserByLogin(login);
        if(user == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with login does not exist");
        }

        if(createUpdateUserDto.getFullName() != null){
            user.setFullName(createUpdateUserDto.getFullName());
        }
        if(createUpdateUserDto.getPassword() != null){
            user.setPassword(authenticationService.encode(createUpdateUserDto.getPassword()));
        }
        if(createUpdateUserDto.getAvatar() != null){
            user.setAvatarUUID(createUpdateUserDto.getAvatar());
        }
        if(createUpdateUserDto.getCity() != null){
            user.setCity(createUpdateUserDto.getCity());
        }
        var email = createUpdateUserDto.getEmail();
        var emailExists = userRepository.existsUserByEmail(email);
        if(!user.getEmail().equals(email) && emailExists){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Email already taken");
        }
        if(email != null){
            user.setEmail(email);
        }
        var loginValue = createUpdateUserDto.getLogin();
        if(user.getLogin().equals(loginValue) && userRepository.existsUserByLogin(loginValue)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Login already taken");
        }
        if(loginValue != null){
            user.setLogin(loginValue);
        }
        if(createUpdateUserDto.getDateOfBirth() != null){
            user.setDateOfBirth(createUpdateUserDto.getDateOfBirth());
        }
        if(createUpdateUserDto.getPhoneNumber() != null){
            user.setPhoneNumber(createUpdateUserDto.getPhoneNumber());
        }
        if(createUpdateUserDto.getGender() != null){
            user.setGender(createUpdateUserDto.getGender());
        }
        var savedUser = userRepository.save(user);
        return UserEntityConverter.userDtoFromEntity(savedUser);
    }

    public ResponseEntity<UserDto> login(LoginDto loginDto){
        var user = userRepository.getUserByLogin(loginDto.getLogin());
        if(user == null){
            logger.warn(String.format("Failed to login:  user does not exist; dto: %s", loginDto.toString()));
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid login or password");
        }
        if(!authenticationService.matches(loginDto.getPassword(), user.getPassword())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid login or password");
        }
        var jwt = authenticationService.createJwt(user);
        sendLoginCompleteNotification(user.getUuid());
        return ResponseEntity.ok()
                .header(SecurityConst.HEADER_JWT, jwt)
                .body(UserEntityConverter.userDtoFromEntity(user));
    }

    public UserDto getUserById(String uuid){
        var user = userRepository.findById(uuid);
        if(!user.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return UserEntityConverter.userDtoFromEntity(user.get());
    }

    public UserDto getUserFullNameById(String uuid){
        var users = userRepository.getUserFullNameById(uuid);
        if(users == null || users.size() == 0){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return UserEntityConverter.userDtoFromEntity(users.get(0));
    }


    public PagedView<UserDto> getUsers(GetUsersRequestDto dto){
        var pagination = dto.getPagination();
        if(pagination.getSize() == null){
            pagination.setSize(paginationDefault.getDefaultSize());
        }
        if(pagination.getPage() == null){
            pagination.setPage(1);
        }

        var size = Math.max(Math.min(pagination.getSize(), paginationDefault.getMaxSize()),0);
        var pageIndex = Math.max(pagination.getPage()-1,0);

        var sortParser = new UserSearchSortParser();
        var searchSort = sortParser.parse(new UserSortingFiltersWrapper(dto.getOrders()));
        var pageable = PageRequest.of(pageIndex, size, searchSort);

        var searchSpecification = UserSpecifications.userSearch(
                dto.getLogin(),
                dto.getEmail(),
                dto.getFullName(),
                dto.getDateOfBirth(),
                dto.getPhoneNumber(),
                dto.getCity(),
                dto.getDateOfBirthComparator()
        );
        var users = userRepository.findAll(searchSpecification, pageable);

        //pagination.setTotal((int)Math.ceil((float)(users.size()) / (float)size));
        pagination = Pagination.fromPageable(users.getPageable());
        pagination.setTotal(users.getTotalPages());
        pagination.setPage(pagination.getPage() + 1);

        var result = (List<UserEntity>) users.getContent();
        return new PagedView<UserDto>(
            result
                .stream()
                .map(UserEntityConverter::userDtoFromEntity)
                .collect(Collectors.toList()),
            pagination
        );
    }

    public List<UserDto> getUsers(List<String> ids){
        var found = userRepository.findAllById(ids);
        return found
                .stream()
                .map(UserEntityConverter::userDtoFromEntity)
                .collect(Collectors.toList());
    }

    private void sendLoginCompleteNotification(String userId){
        streamBridge.send("userLoginCompleted-out-0", userId);
    }

}
