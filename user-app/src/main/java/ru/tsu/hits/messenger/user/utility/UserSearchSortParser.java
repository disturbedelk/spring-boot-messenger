package ru.tsu.hits.messenger.user.utility;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;
import ru.tsu.hits.messenger.user.dto.GetUsersRequestDto;

import static ru.tsu.hits.messenger.user.utility.UsersSortingField.*;

@NoArgsConstructor
public class UserSearchSortParser {
    private Sort sort;
    public Sort parse(UserSortingFiltersWrapper orders){
        sort = Sort.by("fullName");
        if(orders.getOrder(FULL_NAME) == SortingOrder.DESC){
            sort = sort.descending();
        } else sort = sort.ascending();

        addSortToChain(orders.getOrder(LOGIN), "login");
        addSortToChain(orders.getOrder(EMAIL), "email");
        addSortToChain(orders.getOrder(CITY), "city");
        addSortToChain(orders.getOrder(BIRTHDAY), "dateOfBirth");


        return sort;
    }

    private void addSortToChain(SortingOrder order, String fieldName){
        if(order == SortingOrder.NONE){
            return;
        }

        var nextSort = Sort.by(fieldName);
        if(order == SortingOrder.ASC){
            nextSort = nextSort.ascending();
        }
        else if(order == SortingOrder.DESC){
            nextSort = nextSort.descending();
        }

        sort = sort.and(nextSort);
    }
}
