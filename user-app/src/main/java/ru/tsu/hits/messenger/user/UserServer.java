package ru.tsu.hits.messenger.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import ru.tsu.hits.messenger.common.data.EnableServiceRoutesConfig;
import ru.tsu.hits.messenger.common.security.EnableSecurityConfiguration;
import ru.tsu.hits.messenger.common.utility.EnableInterServiceRequestHandler;
import ru.tsu.hits.messenger.common.data.EnableDefaultPaginationConfiguration;

@ConfigurationPropertiesScan("ru.tsu.hits.messenger.common.security")
@SpringBootApplication
@EnableSecurityConfiguration
@EnableServiceRoutesConfig
@EnableDefaultPaginationConfiguration
@EnableInterServiceRequestHandler
public class UserServer {
	public static void main(String[] args) {
		SpringApplication.run(UserServer.class, args);
	}
}
