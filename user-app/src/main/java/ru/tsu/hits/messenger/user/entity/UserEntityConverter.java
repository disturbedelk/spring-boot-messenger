package ru.tsu.hits.messenger.user.entity;

import lombok.experimental.UtilityClass;
import ru.tsu.hits.messenger.user.dto.CreateUpdateUserDto;
import ru.tsu.hits.messenger.user.dto.UserDto;

@UtilityClass
public class UserEntityConverter {
    public static UserEntity userWithFullNameFromQuery(String id, String fullName){
        var user = new UserEntity();
        user.setFullName(fullName);
        user.setUuid(id);
        return user;
    }

    public static UserDto userDtoFromEntity(UserEntity user){
        var dto = new UserDto(
                user.getUuid(),
                user.getFullName(),
                user.getLogin(),
                user.getEmail(),
                user.getPhoneNumber(),
                user.getDateOfBirth(),
                user.getRegistered(),
                user.getCity(),
                user.getAvatarUUID(),
                user.getGender()
        );

        return dto;
    }

    public static UserEntity userFromDto(UserDto dto){
        var entity = new UserEntity(
                dto.getUuid(),
                dto.getLogin(),
                dto.getEmail(),
                "",
                dto.getFullName(),
                dto.getDateOfBirth(),
                dto.getRegistered(),
                dto.getPhoneNumber(),
                dto.getCity(),
                dto.getAvatar(),
                dto.getGender()
        );

        return entity;
    }

    public static UserEntity userFromCreateUpdateDto(CreateUpdateUserDto dto){
        var entity = new UserEntity();
        entity.setLogin(dto.getLogin());
        entity.setEmail(dto.getEmail());
        entity.setPassword(dto.getPassword());
        entity.setFullName(dto.getFullName());
        entity.setDateOfBirth(dto.getDateOfBirth());
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setCity(dto.getCity());
        entity.setAvatarUUID(dto.getAvatar());
        entity.setGender(dto.getGender());

        return entity;
    }
}
