package ru.tsu.hits.messenger.user.repository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import ru.tsu.hits.messenger.user.entity.UserEntity;
import ru.tsu.hits.messenger.user.utility.DateComparisonType;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
public class UserSpecifications {

    public static Specification<UserEntity> userSearch(
            String login,
            String email,
            String fullName,
            Date dateOfBirth,
            String phoneNumber,
            String city,
            DateComparisonType dateOfBirthComparator
    ) {
        return new Specification<UserEntity>() {
            @Override
            public Predicate toPredicate(Root<UserEntity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicates = new ArrayList<>();

                if (login != null && !login.isEmpty()) {
                    predicates.add(cb.like(cb.lower(root.get("login")), "%" + login.toLowerCase() + "%"));
                }

                if (email != null && !email.isEmpty()) {
                    predicates.add(cb.like(cb.lower(root.get("email")), "%" + email.toLowerCase() + "%"));
                }

                if (fullName != null && !fullName.isEmpty()) {
                    predicates.add(cb.like(cb.lower(root.get("fullName")), "%" + fullName.toLowerCase() + "%"));
                }

                if (phoneNumber != null && !phoneNumber.isEmpty()) {
                    predicates.add(cb.equal(root.get("phoneNumber"), phoneNumber));
                }

                if (city != null && !city.isEmpty()) {
                    predicates.add(cb.like(cb.lower(root.get("city")), "%" + city.toLowerCase() + "%"));
                }

                if (dateOfBirth != null) {
                    var comparator = dateOfBirthComparator != null ? dateOfBirthComparator : DateComparisonType.EQUAL;
                    switch (comparator){
                        case EQUAL:
                            predicates.add(cb.equal(root.get("dateOfBirth"), dateOfBirth));
                            break;
                        case AFTER:
                            predicates.add(cb.greaterThanOrEqualTo(root.<Date>get("dateOfBirth"), dateOfBirth));
                            break;
                        case BEFORE:
                            predicates.add(cb.lessThanOrEqualTo(root.<Date>get("dateOfBirth"), dateOfBirth));
                            break;
                        default: break;
                    }
                }

                return cb.and(predicates.toArray(new Predicate[0]));
            }
        };
    }
}

