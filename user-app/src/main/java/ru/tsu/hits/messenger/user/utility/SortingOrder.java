package ru.tsu.hits.messenger.user.utility;

public enum SortingOrder {
    NONE,
    ASC,
    DESC
}
