package ru.tsu.hits.messenger.user.utility;

public enum  DateComparisonType {
    BEFORE,
    EQUAL,
    AFTER
}
