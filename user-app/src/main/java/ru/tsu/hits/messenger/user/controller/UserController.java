package ru.tsu.hits.messenger.user.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.tsu.hits.messenger.common.data.PagedView;
import ru.tsu.hits.messenger.common.security.data.JwtUserData;
import ru.tsu.hits.messenger.user.dto.CreateUpdateUserDto;
import ru.tsu.hits.messenger.user.dto.GetUsersRequestDto;
import ru.tsu.hits.messenger.user.dto.LoginDto;
import ru.tsu.hits.messenger.user.dto.UserDto;
import ru.tsu.hits.messenger.user.service.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
@Slf4j
public class UserController {
    private final UserService userService;

    @PostMapping(produces = "application/json")
    public ResponseEntity<UserDto> save(@Valid @RequestBody CreateUpdateUserDto createUpdateUserDto){
        return userService.create(createUpdateUserDto);
    }

    @GetMapping(value = "/{login}", produces = "application/json")
    public UserDto read(Authentication authentication, @PathVariable String login){
        log.info(String.format("Get user data by login request received, current authentication: %s", authentication));
        var userId = ((JwtUserData)authentication.getPrincipal()).getId().toString();
        return userService.readAuthorized(userId, login);
    }

    @PutMapping(value = "/self", produces = "application/json")
    public UserDto update(Authentication authentication, @Valid @RequestBody CreateUpdateUserDto createUpdateUserDto){
        var login = ((JwtUserData)authentication.getPrincipal()).getLogin();
        return userService.update(login, createUpdateUserDto);
    }

    @PutMapping(value ="/{login}", produces = "application/json")
    public UserDto update(Authentication authentication, @PathVariable String login, @Valid @RequestBody CreateUpdateUserDto createUpdateUserDto){
        if(!authentication.getAuthorities().contains("CAN_MODIFY_USER_DATA")){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        return userService.update(login, createUpdateUserDto);
    }

    @PostMapping(value = "/login", produces = "application/json")
    public ResponseEntity<UserDto> login(@Valid @RequestBody LoginDto loginDto){
        return userService.login(loginDto);
    }

    @GetMapping(value = "/{id}/exists", produces = "application/json")
    public UserDto getUserById(@PathVariable String id){
        return userService.getUserFullNameById(id);
    }

    @GetMapping(value = "/me", produces = "application/json")
    public UserDto getSelf(Authentication authentication){
        var id = ((JwtUserData)authentication.getPrincipal()).getId();
        return userService.getUserById(String.valueOf(id));
    }

    @GetMapping
    public PagedView<UserDto> getUsers(@Valid @RequestBody GetUsersRequestDto requestDto) {
        return userService.getUsers(requestDto);
    }
}
