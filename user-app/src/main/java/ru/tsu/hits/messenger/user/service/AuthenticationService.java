package ru.tsu.hits.messenger.user.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.tsu.hits.messenger.common.security.props.SecurityProps;
import ru.tsu.hits.messenger.user.entity.UserEntity;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.*;

import static java.lang.System.currentTimeMillis;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthenticationService {
    private static final SecureRandom random = new SecureRandom();
    private static final Integer ITERATIONS = 65536;
    private static final Integer KEY_LENGTH = 128;
    private static final Integer SALT_LENGTH_BYTES = 16;
    private static final String PBKDF2_ALG = "PBKDF2WithHmacSHA1";
    private final SecurityProps securityProps;
    public String encode(String password){
        byte[] salt = new byte[SALT_LENGTH_BYTES];
        random.nextBytes(salt);
        return Base64.getEncoder().encodeToString(salt) + " " + hash(password, salt);
    }

    public boolean matches(String password, String hashedPassword){
        var split = hashedPassword.split("\\s");

        byte[] salt = Base64.getDecoder().decode(split[0]);
        String encodedPassword = split[1];

        return hash(password, salt).equals(encodedPassword);
    }

    private String hash(String password, byte[] salt){
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, ITERATIONS, KEY_LENGTH);
        try {
            var factory = SecretKeyFactory.getInstance(PBKDF2_ALG);
            byte[] hash = factory.generateSecret(spec).getEncoded();
            return Base64.getEncoder().encodeToString(hash);
        }
        catch (NoSuchAlgorithmException ignore) {
            return null;
        }
        catch (InvalidKeySpecException e) {
            throw new IllegalArgumentException(e);
        }
    }
    public String createJwt(UserEntity user) {
        var key = Keys.hmacShaKeyFor(securityProps.getJwtToken().getSecret().getBytes(StandardCharsets.UTF_8));
        return Jwts.builder()
            .setSubject(user.getFullName())
            .setClaims(Map.of(
                    "id", user.getUuid(),
                    "name", user.getFullName(),
                    "login", user.getLogin()
            ))
            .setId(UUID.randomUUID().toString())
            .setExpiration(new Date(currentTimeMillis() + securityProps.getJwtToken().getExpiration()))
            .signWith(key, SignatureAlgorithm.HS256)
            .compact();
    }
}
