--
-- PostgreSQL database dump
--

-- Dumped from database version 14.6
-- Dumped by pg_dump version 15.1 (Debian 15.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: users; Type: TABLE; Schema: public; Owner: user-service
--

CREATE TABLE public.users (
    id character varying(255) NOT NULL,
    date_of_birth date,
    full_name character varying(255),
    gender character varying(255),
    login character varying(255),
    password character varying(255),
    registered timestamp without time zone,
    avatar_uuid character varying(255),
    city character varying(255),
    email character varying(255),
    phone_number character varying(255)
);


ALTER TABLE public.users OWNER TO "user-service";

--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: user-service
--

COPY public.users (id, date_of_birth, full_name, gender, login, password, registered, avatar_uuid, city, email, phone_number) FROM stdin;
4256f84c-6276-4f69-a9e1-0ef26233f379	1989-02-15	Bob The Builder	MALE	test	IfQNpRtuNeqyffMYrp6avA== fkHdvX+vmudI7Io6E2CXcQ==	2023-04-21 08:36:59.21	\N	\N	\N	\N
\.


--
-- Name: users uk_6dotkott2kjsp8vw4d0m25fb7; Type: CONSTRAINT; Schema: public; Owner: user-service
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT uk_6dotkott2kjsp8vw4d0m25fb7 UNIQUE (email);


--
-- Name: users uk_ow0gan20590jrb00upg3va2fn; Type: CONSTRAINT; Schema: public; Owner: user-service
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT uk_ow0gan20590jrb00upg3va2fn UNIQUE (login);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: user-service
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

