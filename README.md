Мультимодульный API мессенджера на Spring Boot.  
Предусмотрен сервис отправки картинок и мелких файлов при помощи контейнера Minio.  
Для аутентификации использована Custom JWT Filter Chain.  
Для синхронизации и поточной обработки сообщений базовый сервис интеграции с RabbitMQ  
ORM Hibernate.
