package ru.tsu.hits.messenger.storage.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.tsu.hits.messenger.storage.entity.FileAccessType;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileRecordDto {
    private String uuid;

    private String realName;

    private String extension;

    private String owner;

    private FileAccessType access;

    private List<String> sharedUserIds;
}
