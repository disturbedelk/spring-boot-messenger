package ru.tsu.hits.messenger.storage.entity;

public enum FileAccessType {
    PUBLIC,
    PRIVATE,
    AVATAR
}
