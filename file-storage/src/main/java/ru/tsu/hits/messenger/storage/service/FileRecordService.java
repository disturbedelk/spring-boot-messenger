package ru.tsu.hits.messenger.storage.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import ru.tsu.hits.messenger.storage.dto.FileDto;
import ru.tsu.hits.messenger.storage.dto.FileRecordDto;
import ru.tsu.hits.messenger.storage.dto.GetFilesInfoRequestBody;
import ru.tsu.hits.messenger.storage.dto.ShareFilesRequestBody;
import ru.tsu.hits.messenger.storage.entity.FileAccessType;
import ru.tsu.hits.messenger.storage.entity.FileRecord;
import ru.tsu.hits.messenger.storage.repository.FileRecordRepository;
import ru.tsu.hits.messenger.storage.utility.FileRecordConverter;

import java.io.IOException;
import java.util.*;

import static ru.tsu.hits.messenger.storage.entity.FileAccessType.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class FileRecordService {
    private final FileService fileService;
    private final FileRecordRepository records;
    public FileDto download(String id){
        return download(id, "");
    }

    public FileDto download(String id, String requestingUserId){
        var found = records.findById(id);

        if(found.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        var record = found.get();
        if(!hasAccess(requestingUserId, record)){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "File access restricted");
        }
        var data = fileService.download(id);
        return new FileDto(record.getRealName(), record.getExtension(), data);
    }

    @Transactional
    public FileRecordDto upload(MultipartFile file, FileRecordDto recordDto, String userId){
        var record = new FileRecord(
                UUID.randomUUID().toString(),
                recordDto.getRealName(),
                recordDto.getExtension(),
                userId,
                recordDto.getAccess(),
                recordDto.getSharedUserIds()
        );

        var saved = records.save(record);
        try {
            fileService.upload(file.getBytes());
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Failed to read file");
        }

        return FileRecordConverter.dtoFromEntity(saved);
    }

    public FileRecordDto upload(MultipartFile file){
        var id = UUID.randomUUID().toString();
        var processedName = file.getName().split("\\.(?=[^\\.]+$)");
        var record = new FileRecordDto(id, processedName[0], processedName[1], "public", PUBLIC, null);
        return upload(file, record, record.getOwner());
    }

    private boolean hasAccess(String userId, FileRecord record){
        var access = record.getAccess();
        if(access == PUBLIC || record.getOwner() == userId){
            return true;
        }

        if(access != PRIVATE){// if the case is not considered then decline access per whitelist strategy
            return false;
        }

        //PRIVATE
        var whitelist = record.getSharedUserIds();
        if(whitelist == null || whitelist.size() == 0){
            return false;
        }
        return whitelist.contains(userId);
    }

    public List<FileRecordDto> getExistingFiles(List<String> fileIds) {
        Optional<FileRecord> found;
        List<FileRecordDto> rValue = new ArrayList<>();
        for (var id: fileIds) {
            found = records.findById(id);
            if(found.isEmpty()){
                continue;
            }
            rValue.add(FileRecordConverter.dtoFromEntity(found.get()));
        }

        return rValue;
    }

    @Transactional
    public List<String> shareFiles(ShareFilesRequestBody body) {
        Optional<FileRecord> found;
        FileRecord record;
        List<String> rValue = new ArrayList<>();
        for (var fileId:body.getFileIds()) {
            found = records.findById(fileId);
            if(found.isEmpty()){
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to share file {id:"+fileId+"}");
            }
            record = found.get();
            if(record.getAccess() == PUBLIC){
                continue;
            }

            if(record.getAccess() == PRIVATE && !record.getOwner().equals(body.getRequestingUserId())){
                throw new ResponseStatusException(HttpStatus.FORBIDDEN);
            }

            record.getSharedUserIds().addAll(body.getUsersToShare());
            records.save(record);
            rValue.add(fileId);
        }

        return rValue;
    }
}
