package ru.tsu.hits.messenger.storage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import ru.tsu.hits.messenger.common.security.EnableSecurityConfiguration;

@ConfigurationPropertiesScan("ru.tsu.hits.messenger")
@EnableSecurityConfiguration
@SpringBootApplication
public class FileServer {
    public static void main(String[] args) {
        SpringApplication.run(FileServer.class);
    }
}