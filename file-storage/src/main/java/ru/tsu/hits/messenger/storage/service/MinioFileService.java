package ru.tsu.hits.messenger.storage.service;

import io.minio.GetObjectArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.tsu.hits.messenger.storage.minio.MinioConfig;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class MinioFileService implements FileService {
    private final MinioClient minioClient;
    private final MinioConfig minioConfig;


    @PostConstruct
    public void init(){
        log.info("Minio configs:{}", minioConfig);
    }
    @Override
    public String upload(byte[] content) {
        try {
            var id = UUID.randomUUID().toString();
            minioClient.putObject(PutObjectArgs.builder()
                    .bucket(minioConfig.getBucket())
                    .object(id)
                    .stream(new ByteArrayInputStream(content), content.length, -1)
                    .build());
            return id;
        } catch (Exception e) {
            throw new RuntimeException("Error uploading to file server: ", e);
        }
    }

    @Override
    public byte[] download(String id) {
        return getFile(id);
    }

    private byte[] getFile(String id){
        var args = GetObjectArgs.builder()
                .bucket(minioConfig.getBucket())
                .object(id)
                .build();

        try (var in = minioClient.getObject(args)){
            return in.readAllBytes();
        } catch (Exception e){
            throw new RuntimeException(String.format("Error downloading file from server {id: %s}", id), e);
        }
    }
}
