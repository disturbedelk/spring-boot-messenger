package ru.tsu.hits.messenger.storage.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "records")
@NoArgsConstructor
@AllArgsConstructor
public class FileRecord {
    @Id
    @Column(name = "id")
    @NonNull
    private String uuid;

    @Column(name = "file_name")
    @NonNull
    private String realName;

    @Column(name = "file_extension")
    @NonNull
    private String extension;

    @Column
    @NonNull
    private String owner;

    @Column
    @Enumerated(EnumType.STRING)
    private FileAccessType access;

    @ElementCollection
    @CollectionTable(name = "shared_user_ids", joinColumns = @JoinColumn(name="id"))
    @Column(name = "shared")
    private List<String> sharedUserIds;
}
