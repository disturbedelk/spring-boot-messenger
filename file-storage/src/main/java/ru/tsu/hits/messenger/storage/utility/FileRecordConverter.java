package ru.tsu.hits.messenger.storage.utility;

import lombok.experimental.UtilityClass;
import ru.tsu.hits.messenger.storage.dto.FileRecordDto;
import ru.tsu.hits.messenger.storage.entity.FileRecord;

@UtilityClass
public class FileRecordConverter {
    public  FileRecord entityFromDto(FileRecordDto dto){
        return new FileRecord(dto.getUuid(),
                dto.getRealName(),
                dto.getExtension(),
                dto.getOwner(),
                dto.getAccess(),
                dto.getSharedUserIds()
        );
    }

    public FileRecordDto dtoFromEntity(FileRecord record){
        return new FileRecordDto(record.getUuid(),
                record.getRealName(),
                record.getExtension(),
                record.getOwner(),
                record.getAccess(),
                record.getSharedUserIds()
        );
    }
}
