package ru.tsu.hits.messenger.storage.controller;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import ru.tsu.hits.messenger.common.security.data.JwtUserData;
import ru.tsu.hits.messenger.storage.dto.FileRecordDto;
import ru.tsu.hits.messenger.storage.service.FileRecordService;
import ru.tsu.hits.messenger.storage.service.FileService;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/api/files")
@RequiredArgsConstructor
public class FileController {
    private final FileRecordService fileService;

    @PostMapping("/upload")
    public FileRecordDto upload(@RequestParam("file") MultipartFile file, @RequestBody(required = false) FileRecordDto fileRecordDto, Authentication authentication){
        var userId = ((JwtUserData)authentication.getPrincipal()).getId();
        return fileService.upload(file, fileRecordDto, userId.toString());
    }

    @SneakyThrows
    @GetMapping(value = "/download/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> download(@PathVariable("id") String id, Authentication authentication){
        var userId = ((JwtUserData)authentication.getPrincipal()).getId();
        var file = fileService.download(id, userId.toString());
        return  ResponseEntity.ok()
                .header("Content-Disposition", "filename=" + file.getName() + "." + file.getExtension())
                .body(file.getData());
    }

    @SneakyThrows
    @GetMapping(value = "/download/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> download(@PathVariable("id") String id){
        var file = fileService.download(id);
        return  ResponseEntity.ok()
                .header("Content-Disposition", "filename=" + file.getName() + "." + file.getExtension())
                .body(file.getData());
    }
}
