package ru.tsu.hits.messenger.storage.controller;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.tsu.hits.messenger.common.security.data.JwtUserData;
import ru.tsu.hits.messenger.storage.dto.FileRecordDto;
import ru.tsu.hits.messenger.storage.dto.GetFilesInfoRequestBody;
import ru.tsu.hits.messenger.storage.dto.ShareFilesRequestBody;
import ru.tsu.hits.messenger.storage.service.FileRecordService;
import ru.tsu.hits.messenger.storage.service.FileService;

import java.util.List;

@RestController
@RequestMapping("/integration/files")
@RequiredArgsConstructor
public class FileIntegrationController {
    private final FileRecordService fileService;
    @PostMapping("/upload")
    public FileRecordDto upload(@RequestParam("file") MultipartFile file, @RequestBody(required = false) FileRecordDto fileRecordDto){
        var userId = fileRecordDto.getOwner();
        userId = userId == null ? "" : userId;
        return fileService.upload(file, fileRecordDto, userId);
    }

    @SneakyThrows
    @GetMapping(value = "/download/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> download(@PathVariable("id") String id){
        var file = fileService.download(id);
        return  ResponseEntity.ok()
                .header("Content-Disposition", "filename=" + file.getName() + "." + file.getExtension())
                .body(file.getData());
    }

    @GetMapping("/get-existing-info")
    public List<FileRecordDto> getExistingFilesInfo(@RequestBody GetFilesInfoRequestBody body){
        return fileService.getExistingFiles(body.getFileIds());
    }

    @PostMapping("/share-files-with-users")
    public List<String> shareFilesWithUsers(@RequestBody ShareFilesRequestBody body){
        return fileService.shareFiles(body);
    }
}
