package ru.tsu.hits.messenger.storage.service;

public interface FileService {
    String upload(byte[] content);
    byte[] download(String id);
}
