package ru.tsu.hits.messenger.storage.repository;

import org.springframework.data.repository.CrudRepository;
import ru.tsu.hits.messenger.storage.entity.FileRecord;

public interface FileRecordRepository extends CrudRepository<FileRecord, String> {
}
