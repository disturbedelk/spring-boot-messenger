package ru.tsu.hits.messenger.storage.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileDto {
    private String name;
    private String extension;
    private byte[] data;
}
