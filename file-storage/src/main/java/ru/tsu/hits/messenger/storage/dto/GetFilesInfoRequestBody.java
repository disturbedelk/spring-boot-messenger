package ru.tsu.hits.messenger.storage.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetFilesInfoRequestBody {
    private List<String> fileIds;
    private String requestingUserId;
}
