package ru.tsu.hits.messenger.chat.dto.restClient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private String uuid;
    private String fullName;
    private String login;
    private String email;
    private String avatar;
}
