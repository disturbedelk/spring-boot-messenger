package ru.tsu.hits.messenger.chat.repository;

import lombok.NonNull;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.lang.Nullable;
import ru.tsu.hits.messenger.chat.entity.*;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import java.util.List;
import java.util.Optional;

public interface ChatRepository<T extends Chat> extends JpaRepository<T, String>, JpaSpecificationExecutor<T> {
    default Optional<T> findDialogueByUserIds(String userId, String targetUserId){
        Specification<T> specification = (root, query, cb) -> {
            root.fetch(Chat_.users);
            return cb.and(
                    cb.equal(root.get(Chat_.TYPE), ChatType.DIALOGUE.name()),
                    cb.equal(root.get(Chat_.USERS).get(ChatUser_.USER_ID), targetUserId),
                    cb.equal(root.get(Chat_.USERS).get(ChatUser_.USER_ID), userId)
            );
        };

        return findOne(specification);
    }

    default Page<T> findAllUsersChats(String userId, Pageable pageable){
        Specification<T> specification = (root, query, cb) -> {
            root.fetch(Chat_.users);
            return cb.equal(root.get(Chat_.USERS).get(ChatUser_.USER_ID), userId);
        };
        return findAll(specification, pageable);
    }

    default List<T> findAllUsersChats(String userId){
        Specification<T> specification = (root, query, cb) -> {
            root.fetch(Chat_.users);
            return cb.equal(root.get(Chat_.USERS).get(ChatUser_.USER_ID), userId);
        };
        return findAll(specification);
    }
}
