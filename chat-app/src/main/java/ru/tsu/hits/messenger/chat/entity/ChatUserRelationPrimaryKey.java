package ru.tsu.hits.messenger.chat.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatUserRelationPrimaryKey implements Serializable {
    private String chatId;
    private String userId;
}
