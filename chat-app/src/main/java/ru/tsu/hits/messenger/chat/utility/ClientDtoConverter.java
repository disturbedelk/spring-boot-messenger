package ru.tsu.hits.messenger.chat.utility;

import lombok.experimental.UtilityClass;
import ru.tsu.hits.messenger.chat.dto.restClient.FileRecordDto;
import ru.tsu.hits.messenger.chat.entity.Attachment;

@UtilityClass
public class ClientDtoConverter {
    public Attachment fileRecordDtoToAttachment(FileRecordDto dto){
        var rValue = new Attachment();
        rValue.setFileRecordId(dto.getUuid());
        rValue.setName(dto.getRealName() + "." + dto.getExtension());
        return rValue;
    }
}
