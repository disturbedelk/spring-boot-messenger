package ru.tsu.hits.messenger.chat.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageSearchRequestBodyDto {
    private String wildcard;
}
