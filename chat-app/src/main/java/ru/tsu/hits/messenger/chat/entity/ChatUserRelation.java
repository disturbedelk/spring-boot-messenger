package ru.tsu.hits.messenger.chat.entity;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "chats_users")
@NoArgsConstructor
@AllArgsConstructor
@IdClass(ChatUserRelationPrimaryKey.class)
public class ChatUserRelation {
    @Id
    @Column(name = "chat_id")
    private String chatId;
    @Id
    @Column(name = "user_id")
    private String userId;
}
