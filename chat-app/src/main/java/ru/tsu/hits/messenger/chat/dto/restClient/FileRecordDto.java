package ru.tsu.hits.messenger.chat.dto.restClient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileRecordDto {
    private String uuid;

    private String realName;

    private String extension;

    private String owner;

    private List<String> sharedUserIds;
    private String access;
}