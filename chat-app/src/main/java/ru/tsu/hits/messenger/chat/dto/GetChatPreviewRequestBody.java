package ru.tsu.hits.messenger.chat.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.hits.messenger.common.data.Pagination;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetChatPreviewRequestBody {
    private Pagination pagination;
    private String chatNameWildcard;
}
