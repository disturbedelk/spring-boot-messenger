package ru.tsu.hits.messenger.chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import ru.tsu.hits.messenger.common.data.EnableServiceRoutesConfig;
import ru.tsu.hits.messenger.common.security.EnableSecurityConfiguration;
import ru.tsu.hits.messenger.common.security.props.EnableSecurityProps;
import ru.tsu.hits.messenger.common.utility.EnableInterServiceRequestHandler;

@ConfigurationPropertiesScan("ru.tsu.hits.messenger.common.security")
@EnableSecurityConfiguration
@EnableSecurityProps
@EnableInterServiceRequestHandler
@EnableServiceRoutesConfig
@SpringBootApplication
public class ChatServer {

	public static void main(String[] args) {
		SpringApplication.run(ChatServer.class, args);
	}

}
