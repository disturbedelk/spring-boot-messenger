package ru.tsu.hits.messenger.chat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsu.hits.messenger.chat.entity.ChatUser;

public interface ChatUserRepository extends JpaRepository<ChatUser, String> {
}
