package ru.tsu.hits.messenger.chat.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatPreviewListElementDto {
    private String id;
    private String name;
    private String text;
    private Boolean containsAttachments;
    private Date sent;
    private String senderId;
}
