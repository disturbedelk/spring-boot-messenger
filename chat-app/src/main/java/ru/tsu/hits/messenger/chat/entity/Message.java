package ru.tsu.hits.messenger.chat.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "messages")
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    @Id
    @GeneratedValue
    private String id;

    @NonNull
    @Column(name = "sender_id")
    private String senderId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "chat_id")
    @NonNull
    private Chat chat;

    @Column(name = "sent")
    @Temporal(TemporalType.TIMESTAMP)
    @NonNull
    private Date sent;

    @Column(name = "message_content", length = 500)
    @Lob
    @NonNull
    private String text;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "message")
    private List<Attachment> attachments;
}
