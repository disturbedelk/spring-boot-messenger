package ru.tsu.hits.messenger.chat.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GroupChat extends Chat{
    @Column(name = "chat_name")
    private String name;

    @Column(name = "administrator_id")
    private String adminId;

    @Column(name = "date_created")
    @Temporal(TemporalType.DATE)
    private Date created;

    @Column(name = "chat_avatar_id")
    private String avatarId;
}
