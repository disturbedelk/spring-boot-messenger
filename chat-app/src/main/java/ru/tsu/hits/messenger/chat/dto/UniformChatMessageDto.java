package ru.tsu.hits.messenger.chat.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UniformChatMessageDto {
    private String id;
    private Date sent;
    private String text;
    private String senderName;
    private String senderAvatarId;
    private List<AttachmentDto> attachments;

}
