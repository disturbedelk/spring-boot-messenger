package ru.tsu.hits.messenger.chat.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateUpdateGroupChatDto {
    private String chatId;
    private String ownerId;
    private String avatarId;
    private String name;
    private List<String> users;
}
