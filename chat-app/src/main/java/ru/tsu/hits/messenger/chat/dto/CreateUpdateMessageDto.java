package ru.tsu.hits.messenger.chat.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateUpdateMessageDto {
    private String text;
    private List<String> attachmentIds;
}
