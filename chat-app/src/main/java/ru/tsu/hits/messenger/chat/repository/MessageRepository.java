package ru.tsu.hits.messenger.chat.repository;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import ru.tsu.hits.messenger.chat.entity.ChatUser_;
import ru.tsu.hits.messenger.chat.entity.Chat_;
import ru.tsu.hits.messenger.chat.entity.Message;
import ru.tsu.hits.messenger.chat.entity.Message_;
//import ru.tsu.hits.messenger.chat.entity.Message_;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface MessageRepository extends JpaRepository<Message, String>, JpaSpecificationExecutor<Message> {
    @Query(value = "SELECT DISTINCT sender_id FROM messages WHERE chat = ?1", nativeQuery = true)
    List<String> findAllSenderIdsByChatId(String chatId);

    default Optional<Message> findLastSentMessageByChatId(String chatId){
        Specification<Message> specification = (root, query, cb) -> {
            //query.groupBy(root.get(Message_.chat));
            query.orderBy(cb.desc(root.get(Message_.sent)));
            return cb.equal(root.get(Message_.chat), chatId);
        };
        return findOne(specification);
    }

    default List<Message> findAllUserMessagesWithWildcardText(String requestingUserId, String wildcard){
        if(wildcard == null){
            return new ArrayList<>();
        }
        Specification<Message> specification = (root, query, cb) -> {
            root.fetch(Message_.chat).fetch(Chat_.users);
            var chatUser = root.get(Message_.CHAT).get(Chat_.USERS);
            return cb.and(
                    cb.equal(chatUser.get(ChatUser_.ID), requestingUserId),
                    cb.equal(chatUser.get(ChatUser_.REMOVED), false),
                    cb.like(root.get(Message_.TEXT), "%" + wildcard.toLowerCase() + "%")
            );
        };
        return findAll(specification);
    }
}
