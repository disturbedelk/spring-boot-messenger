package ru.tsu.hits.messenger.chat.utility;

import lombok.experimental.UtilityClass;
import ru.tsu.hits.messenger.chat.dto.AttachmentDto;
import ru.tsu.hits.messenger.chat.dto.ChatInfoDto;
import ru.tsu.hits.messenger.chat.dto.CreateUpdateGroupChatDto;
import ru.tsu.hits.messenger.chat.entity.Attachment;
import ru.tsu.hits.messenger.chat.entity.Chat;
import ru.tsu.hits.messenger.chat.entity.ChatUser;
import ru.tsu.hits.messenger.chat.entity.GroupChat;

import java.util.stream.Collectors;

@UtilityClass
public class ChatEntityDtoConverter {
    public CreateUpdateGroupChatDto groupChatDtoFromEntity(GroupChat chat){
        return new CreateUpdateGroupChatDto(chat.getId(),
                chat.getAdminId(),
                chat.getAvatarId(),
                chat.getName(),
                chat.getUsers().stream().map(ChatUser::getUserId).collect(Collectors.toList())
        );
    }

    public AttachmentDto attachmentDtoFromEntity(Attachment attachment){
        return new AttachmentDto(attachment.getFileRecordId(), attachment.getName(), attachment.getFileSize());
    }
}
