package ru.tsu.hits.messenger.chat.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;

@Entity
@Data
@Table(name = "chats_users")
@AllArgsConstructor
@NoArgsConstructor
public class ChatUser {
    @Id
    @GeneratedValue
    private String id;
    @NonNull
    private String userId;
    @ManyToOne(fetch = FetchType.LAZY)
    private Chat chat;
    private boolean removed = false;
    private String avatarId;
    private String name;
}
