package ru.tsu.hits.messenger.chat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsu.hits.messenger.chat.entity.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment, String> {
}
