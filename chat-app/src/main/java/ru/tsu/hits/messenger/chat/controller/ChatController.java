package ru.tsu.hits.messenger.chat.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.tsu.hits.messenger.chat.dto.*;
import ru.tsu.hits.messenger.chat.service.ChatService;
import ru.tsu.hits.messenger.common.data.PagedView;
import ru.tsu.hits.messenger.common.security.data.JwtUserData;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/chat")
@RequiredArgsConstructor
public class ChatController {
    private final ChatService chatService;

    @PostMapping("/personal")
    private PersonalMessageDto sendMessagePersonal(@RequestBody @Valid PersonalMessageDto personalMessageDto, Authentication authentication){
        var userId = ((JwtUserData)authentication.getPrincipal()).getId();
        return chatService.sendPersonalMessage(personalMessageDto, userId.toString());
    }

    @PostMapping("/group")
    private GroupMessageDto sendMessageToGroupChat(@RequestBody @Valid GroupMessageDto groupMessageDto, Authentication authentication){
        var userId = ((JwtUserData)authentication.getPrincipal()).getId();
        return chatService.sendGroupChatMessage(groupMessageDto, userId.toString());
    }

    @PostMapping("/group/create")
    private CreateUpdateGroupChatDto createGroupChat(@RequestBody @Valid CreateUpdateGroupChatDto groupChatDto, Authentication authentication){
        var userId = ((JwtUserData)authentication.getPrincipal()).getId();
        groupChatDto.setOwnerId(userId.toString());
        return chatService.createGroupChat(groupChatDto);
    }

    @PostMapping("/group/update")
    private CreateUpdateGroupChatDto updateGroupChat(@RequestBody @Valid CreateUpdateGroupChatDto groupChatDto, Authentication authentication){
        var userId = ((JwtUserData)authentication.getPrincipal()).getId();
        return chatService.updateGroupChat(groupChatDto, userId.toString());
    }

    @GetMapping("/{id}/info")
    private ChatInfoDto getChatInfo(@PathVariable(name = "id") UUID chatId, Authentication authentication){
        var userId = ((JwtUserData)authentication.getPrincipal()).getId();
        return chatService.getChatInfo(chatId.toString(), userId.toString());
    }

    @GetMapping("/{id}/")
    private List<UniformChatMessageDto> getChatContents(@PathVariable(name = "id") UUID chatId, Authentication authentication){
        var userId = ((JwtUserData)authentication.getPrincipal()).getId();
        return chatService.getChatContents(chatId.toString(), userId.toString());
    }

    @GetMapping("/list")
    private PagedView<ChatPreviewListElementDto> getChatPreviewList(@RequestBody GetChatPreviewRequestBody body, Authentication authentication){
        var userId = ((JwtUserData)authentication.getPrincipal()).getId();
        return chatService.getChatPreviewList(body, userId.toString());
    }

    @GetMapping("/search")
    private List<MessageSearchListElementDto> searchMessages(@RequestBody MessageSearchRequestBodyDto body, Authentication authentication){
        var userId = ((JwtUserData)authentication.getPrincipal()).getId();
        return chatService.searchMessages(body, userId.toString());
    }
}
