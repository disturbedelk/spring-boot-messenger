package ru.tsu.hits.messenger.chat.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import ru.tsu.hits.messenger.chat.dto.*;
import ru.tsu.hits.messenger.chat.dto.restClient.FileRecordDto;
import ru.tsu.hits.messenger.chat.dto.restClient.FileRecordListRequestBody;
import ru.tsu.hits.messenger.chat.dto.restClient.ShareFilesRequestBody;
import ru.tsu.hits.messenger.chat.dto.restClient.UserDto;
import ru.tsu.hits.messenger.chat.entity.*;
import ru.tsu.hits.messenger.chat.repository.AttachmentRepository;
import ru.tsu.hits.messenger.chat.repository.ChatRepository;
import ru.tsu.hits.messenger.chat.repository.ChatUserRepository;
import ru.tsu.hits.messenger.chat.repository.MessageRepository;
import ru.tsu.hits.messenger.chat.utility.ChatEntityDtoConverter;
import ru.tsu.hits.messenger.chat.utility.ClientDtoConverter;
import ru.tsu.hits.messenger.common.data.PagedView;
import ru.tsu.hits.messenger.common.data.Pagination;
import ru.tsu.hits.messenger.common.utility.InterServiceRequestHandler;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
@RequiredArgsConstructor
public class ChatService {
    private final ChatRepository<DialogueChat> dialogueRepository;
    private final ChatRepository<GroupChat> groupChatChatRepository;
    private final ChatRepository<Chat> combinedChatRepository;
    private final MessageRepository messageRepository;
    private final AttachmentRepository attachmentRepository;
    private final InterServiceRequestHandler requestHandler;
    private final ChatUserRepository chatUserRepository;

    @Transactional
    public PersonalMessageDto sendPersonalMessage(PersonalMessageDto personalMessageDto, String userId) {
        var found = dialogueRepository.findDialogueByUserIds(userId, personalMessageDto.getTargetUserId());
        log.info("retrieved dialogue between users exists: " + found.isPresent());
        DialogueChat dialogue = found.orElseGet(() -> createPersonalMessageDialogue(personalMessageDto, userId));
        dialogue = found.map(
                chat -> (DialogueChat) chat
        ).orElseGet(
                () -> createPersonalMessageDialogue(personalMessageDto, userId)
        );

        List<Attachment> attachments = getAttachments(personalMessageDto.getAttachmentIds(), userId);

        try {
            shareFiles(
                attachments.stream().map(Attachment::getFileRecordId).collect(Collectors.toList()),
                userId,
                Arrays.asList(personalMessageDto.getTargetUserId())
            );
        } catch (Exception ex){
            log.warn("Personal message request failed to share some attachments with recipients: " + ex.getMessage());
        }

        var message = new Message(
                UUID.randomUUID().toString(),
                userId,
                dialogue,
                new Date(),
                personalMessageDto.getText(),
                attachments
        );

        attachments = attachments.stream().map(x -> {
            x.setMessage(message);
            x.setId(UUID.randomUUID().toString());
            return x;
        }).collect(Collectors.toList());


        var savedAttachments = attachmentRepository.saveAll(attachments);
//        message.setAttachments(savedAttachments);
        var savedMessage = messageRepository.save(message);

        personalMessageDto.setAttachmentIds(
                attachments.stream().map(
                        Attachment::getFileRecordId
                ).collect(Collectors.toList())
        );
        return personalMessageDto;
    }


    public GroupMessageDto sendGroupChatMessage(GroupMessageDto groupMessageDto, String userId) {
        var found = groupChatChatRepository.findById(groupMessageDto.getTargetChatId());
        GroupChat targetChat = found.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Requested chat not found"));

        List<Attachment> attachments = getAttachments(groupMessageDto.getAttachmentIds(), userId);

        try {
            shareFiles(
                    attachments.stream().map(Attachment::getFileRecordId).collect(Collectors.toList()),
                    userId,
                    targetChat.getUsers().stream().map(ChatUser::getUserId).collect(Collectors.toList())
            );
        } catch (Exception ex){
            log.warn("Chat message request failed to share some attachments with recipients: " + ex.getMessage());
        }

        var message = new Message(
                UUID.randomUUID().toString(),
                userId,
                targetChat,
                new Date(),
                groupMessageDto.getText(),
                attachments
        );

        attachments = attachments.stream().map(x -> {
            x.setMessage(message);
            x.setId(UUID.randomUUID().toString());
            return x;
        }).collect(Collectors.toList());


        var savedAttachments = attachmentRepository.saveAll(attachments);
//        message.setAttachments(savedAttachments);
        var savedMessage = messageRepository.save(message);

        groupMessageDto.setAttachmentIds(
                attachments.stream().map(
                        Attachment::getFileRecordId
                ).collect(Collectors.toList())
        );
        return groupMessageDto;
    }

    @Transactional
    public CreateUpdateGroupChatDto createGroupChat(CreateUpdateGroupChatDto groupChatDto) {
        if(groupChatDto.getName() == null || groupChatDto.getName().isBlank()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Name field can not be null or empty");
        }
        if(groupChatDto.getOwnerId() == null || groupChatDto.getOwnerId().isBlank()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Owner id must be filled");
        }

        GroupChat chat = new GroupChat();
        chat.setType(ChatType.GROUP_CHAT);
        chat.setCreated(new Date());
        chat.setAdminId(groupChatDto.getOwnerId());
        chat.setName(groupChatDto.getName());
        chat.setMessages(new ArrayList<>());


        verifyAvatar(groupChatDto, chat);

        chat.setAvatarId(groupChatDto.getAvatarId());

        var usersIncludingOwner = new ArrayList<String>(groupChatDto.getUsers());
        if(!usersIncludingOwner.contains(chat.getAdminId())){
            usersIncludingOwner.add(chat.getAdminId());
        }
        List<UserDto> userData = new ArrayList<>();
        try {
            userData = getUserData(usersIncludingOwner);
        } catch (ResponseStatusException ex){
            log.error(String.format("Failed to verify users while creating a group chat: [%s] %s", ex.getStatus(), ex.getMessage()));
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to check existing users");
        }

        List<ChatUser> users = new ArrayList<>();
        UserDto current;
        for (var userId: usersIncludingOwner) {
            current = userData.stream().filter(x -> x.getUuid() == userId).findFirst().orElse(new UserDto());
            users.add(
                new ChatUser(
                    UUID.randomUUID().toString(),
                    userId,
                    chat,
                    false,
                    current.getAvatar(),
                    current.getFullName())
            );
        }

        chatUserRepository.saveAll(users);
        chat.setUsers(users);
        var saved = groupChatChatRepository.save(chat);
        return ChatEntityDtoConverter.groupChatDtoFromEntity(saved);
    }

    private void verifyAvatar(CreateUpdateGroupChatDto groupChatDto, GroupChat chat) {
        FileRecordDto avatarFileRecord;
        try {
            avatarFileRecord = getFileRecord(groupChatDto.getAvatarId());
        } catch(ResponseStatusException ex){
            if(ex.getStatus() == HttpStatus.NOT_FOUND){
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Avatar file not found");
            }
            log.error("Failed to retrieve avatar file record: [" + ex.getStatus() + "] " + ex.getMessage());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to retrieve avatar file record");
        }

        shareAvatarIfNotPublic(groupChatDto.getUsers(), chat, groupChatDto.getAvatarId(), avatarFileRecord);
    }

    private List<UserDto> getUserData(List<String> userIds) {
        ResponseEntity<List> response = requestHandler.integrationRequest(
                "users",
                HttpMethod.GET,
                List.class,
                "/exist",
                userIds
        );

        if(response.getStatusCode() != HttpStatus.OK
                || response.getBody() == null
                || response.getBody().size() < userIds.size())
        {
            log.error("Failed to retrieve user data");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Failed to retrieve user data");
        }

        return response.getBody();
    }

    private FileRecordDto getFileRecord(String id) {
        ResponseEntity<FileRecordDto> response = requestHandler.integrationRequest(
                "files",
                HttpMethod.GET,
                FileRecordDto.class,
                "/record/"+id,
                null
        );

        if(response.getStatusCode() != HttpStatus.OK){
            var stringBody = response.getBody() != null ? response.getBody().toString() : "";
            throw new ResponseStatusException(response.getStatusCode(), stringBody);
        }

        return response.getBody();
    }

    @Transactional
    public CreateUpdateGroupChatDto updateGroupChat(CreateUpdateGroupChatDto groupChatDto, String requestingUserId) {
        var found = groupChatChatRepository.findById(groupChatDto.getChatId());
        if(found.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        var chat = found.get();
        var chatUserIds = chat.getUsers().stream().map(ChatUser::getUserId).collect(Collectors.toList());

        if(!chat.getAdminId().equals(requestingUserId)){
            if(!chatUserIds.contains(requestingUserId)){
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        var newName = groupChatDto.getName();
        if( newName != null && !newName.isBlank() && !chat.getName().equals(newName)) {
            chat.setName(groupChatDto.getName());
        }

        updateChatAdmin(chat, groupChatDto.getOwnerId());

        var removedUserIds = chatUserIds
                .stream()
                .filter(x -> !groupChatDto.getUsers().contains(x))
                .collect(Collectors.toList());

        if(removedUserIds.contains(chat.getAdminId())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can not remove active chat admin from the group chat");
        }

        chat.setUsers(chat.getUsers()
                .stream()
                .filter(x -> !removedUserIds.contains(x.getUserId()))
                .collect(Collectors.toList())
        );

        chatUserIds.removeAll(removedUserIds);

        var addedUserIds = groupChatDto.getUsers()
                .stream()
                .filter(x -> !chatUserIds.contains(x))
                .collect(Collectors.toList());

        addUsersToGroupChat(chat, addedUserIds);

        var newAvatarId = groupChatDto.getAvatarId();
        if(!newAvatarId.isBlank()){
            verifyAvatar(groupChatDto, chat);
            chat.setAvatarId(newAvatarId);
        }

        var saved = groupChatChatRepository.save(chat);

        return ChatEntityDtoConverter.groupChatDtoFromEntity(saved);
    }

    private void updateChatAdmin(GroupChat chat, String newOwnerId) {
        if(newOwnerId == null
                || newOwnerId.isBlank()
                || chat.getAdminId().equals(newOwnerId)){
            return;
        }

        ResponseEntity<UserDto> response = requestHandler.integrationRequest(
                "users",
                HttpMethod.GET,
                UserDto.class,
                String.format("/%s/exists", newOwnerId),
                null
        );
        var userNotFoundResponseMessage = String.format("Failed to verify new admin user: user with {id:%s} was not found", newOwnerId);
        if(response.getStatusCode() != HttpStatus.OK){
            if(response.getStatusCode() == HttpStatus.NOT_FOUND){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, userNotFoundResponseMessage);
            }
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to retrieve user data for dialogue");
        }

        var user = response.getBody();

        if(user == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, userNotFoundResponseMessage);
        }

        chat.setAdminId(newOwnerId);
        if(!chat.getUsers().stream().map(x -> x.getUserId()).collect(Collectors.toList()).contains(newOwnerId)) {
            var chatUser = new ChatUser(
                    UUID.randomUUID().toString(),
                    newOwnerId,
                    chat,
                    false,
                    user.getAvatar(),
                    user.getFullName()
            );
            chatUserRepository.save(chatUser);
            chat.getUsers().add(chatUser);
        }
    }

    //TODO: verify adequate logic
    public ChatInfoDto getChatInfo(String chatId, String requestingUserId) {
        var found = combinedChatRepository.findById(chatId);
        if(found.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        var chat = found.get();
        if(!chat.getUsers().stream().map(ChatUser::getUserId).collect(Collectors.toList()).contains(requestingUserId)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        String name = "";
        Date created = null;
        if(chat.getType() == ChatType.GROUP_CHAT){
            GroupChat groupChat = (GroupChat)chat;
            name = groupChat.getName();
            created = groupChat.getCreated();
        } else if(chat.getType() == ChatType.DIALOGUE){
            name = getDialogueName(chat, requestingUserId);
        }


        return new ChatInfoDto(name, created);
    }

    private String getDialogueName(Chat chat, String requestingUserId) {
        var targetUserId = chat.getUsers().stream().filter(x -> !x.getUserId().equals(requestingUserId)).findFirst();
        if(targetUserId.isEmpty()){
            return "";
        }
        ResponseEntity<UserDto> response = requestHandler.integrationRequest(
                "users",
                HttpMethod.GET,
                UserDto.class,
                String.format("/%s/exists", targetUserId.get()),
                null
        );
        if(response.getStatusCode() != HttpStatus.OK){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to retrieve target user data for dialogue");
        }

        var user = response.getBody();

        if(user == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Target user for dialogue not found");
        }

        //Coalesce(fullName, login, email)
        return Stream.of(user.getFullName(), user.getLogin(), user.getEmail())
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(null);
    }

    public List<UniformChatMessageDto> getChatContents(String chatId, String requestingUserId) {
        var found = combinedChatRepository.findById(chatId);
        if(found.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Chat with {id:%s} was not found", chatId));
        }
        var chat = found.get();
        //guarantee fetch lazy load

//        var users = chat.getUsers();
        Map<String, ChatUser> users = chat.getUsers().stream().collect(Collectors.toMap(ChatUser::getUserId, Function.identity()));

        var rValue = new ArrayList<UniformChatMessageDto>();
        for (var message: chat.getMessages()) {
            var attachments = message.getAttachments()
                    .stream()
                    .map(ChatEntityDtoConverter::attachmentDtoFromEntity)
                    .collect(Collectors.toList());
            rValue.add(new UniformChatMessageDto(
                message.getId(),
                message.getSent(),
                message.getText(),
                users.get(message.getSenderId()).getName(),
                users.get(message.getSenderId()).getAvatarId(),
                attachments
            ));
        }
        return rValue;
    }

    private HashMap<String, UserDto> retrieveUserData(List<String> userIds) {
        var rValue = new HashMap<String, UserDto>();
        ResponseEntity<UserDto> response;
        for (var user: userIds) {
            response = requestHandler.integrationRequest(
                    "users",
                    HttpMethod.GET,
                    UserDto.class,
                    String.format("/%s/exists", user),
                    null
            );
            if(response.getStatusCode() != HttpStatus.OK
                || response.getBody() == null){
                rValue.put(user, null);
                continue;
            }

            rValue.put(user, response.getBody());
        }
        return rValue;
    }

    public PagedView<ChatPreviewListElementDto> getChatPreviewList(GetChatPreviewRequestBody request, String userId) {
        List<ChatPreviewListElementDto> rValue = new ArrayList<>();
        Optional<Message> foundLastMessage;
        Message lastMessage;
        var chats = combinedChatRepository.findAllUsersChats(userId, request.getPagination().toPageable());
        for (var chat: chats.getContent()) {
            foundLastMessage = messageRepository.findLastSentMessageByChatId(chat.getId());
            lastMessage = foundLastMessage.orElseGet(Message::new);
            String chatName = "";
            if(chat.getType() == ChatType.DIALOGUE){
                chatName = chat.getUsers().stream().filter(x -> x.getId() != userId).findFirst().orElse(new ChatUser()).getName();
            } else {
                chatName = groupChatChatRepository.findById(chat.getId()).orElseThrow().getName();
            }
            rValue.add(
                new ChatPreviewListElementDto(
                        chat.getId(),
                        chatName,
                        lastMessage.getText(),
                        !lastMessage.getAttachments().isEmpty(),
                        lastMessage.getSent(),
                        lastMessage.getSenderId()
                )
            );
        }

        return new PagedView<ChatPreviewListElementDto>(rValue, Pagination.fromPageable(chats.getPageable()));
    }

    public List<MessageSearchListElementDto> searchMessages(MessageSearchRequestBodyDto body, String requestingUserId) {
        var messages = messageRepository.findAllUserMessagesWithWildcardText(requestingUserId, body.getWildcard());
        List<MessageSearchListElementDto> rValue = new ArrayList<>();

        String chatName = "";

        for (var message: messages) {

            if(message.getChat().getType() == ChatType.DIALOGUE){
                chatName = message.getChat().getUsers().stream().filter(x -> x.getId() != requestingUserId).findFirst().orElse(new ChatUser()).getName();
            } else {
                chatName = groupChatChatRepository.findById(message.getChat().getId()).orElseThrow().getName();
            }

            rValue.add(
                new MessageSearchListElementDto(
                    message.getId(),
                    chatName,
                    message.getText(),
                    !message.getAttachments().isEmpty(),
                    message.getSent(),
                    message.getAttachments().stream().map(Attachment::getName).collect(Collectors.toList())
            ));
        }
        return rValue;
    }

    @Transactional
    private DialogueChat createPersonalMessageDialogue(PersonalMessageDto personalMessageDto, String userId){

        var dialogue = new DialogueChat();
        dialogue.setId(UUID.randomUUID().toString());
        dialogue.setType(ChatType.DIALOGUE);
        var userData = getUserData(Arrays.asList(personalMessageDto.getTargetUserId(), userId)).stream().collect(Collectors.toMap(UserDto::getUuid, Function.identity()));
        var users = Arrays.asList(
                new ChatUser(
                        UUID.randomUUID().toString(),
                        personalMessageDto.getTargetUserId(),
                        dialogue,
                        false,
                        userData.get(personalMessageDto.getTargetUserId()).getAvatar(),
                        userData.get(personalMessageDto.getTargetUserId()).getFullName()
                ),
                new ChatUser(
                        UUID.randomUUID().toString(),
                        userId,
                        dialogue,
                        false,
                        userData.get(userId).getAvatar(),
                        userData.get(userId).getFullName()
                )
        );

        chatUserRepository.saveAll(users);
        dialogue.setUsers(users);
        return dialogueRepository.save(dialogue);
    }

    private List<Attachment> getAttachments(List<String> attachmentIds, String requestingUserId){
        var requestBody = new FileRecordListRequestBody(requestingUserId, attachmentIds);
        ResponseEntity<List<FileRecordDto>> response = requestHandler.integrationRequest(
                "files",
                HttpMethod.GET,
                List.class,
                "/get-existing-info",
                requestBody
            );
        List<Attachment> rValue = new ArrayList<Attachment>();
        if(response.getStatusCode() != HttpStatus.OK){
            return rValue;
        }

        var list = response.getBody();

        if(list == null){
            log.error("Failed to retrieve attachments from file server; response body is null: " + (response.getBody() == null));
            return rValue;
        }
        rValue = list.stream().map(ClientDtoConverter::fileRecordDtoToAttachment).collect(Collectors.toList());

        return rValue;
    }

    private void shareFiles(List<String> fileIds, String requestingUserId, List<String> usersToShare){
        var requestBody = new ShareFilesRequestBody(fileIds, requestingUserId, usersToShare);
        var response = requestHandler.integrationRequest(
                "files",
                HttpMethod.POST,
                List.class,
                "/share-files-with-users",
                requestBody
        );
        if(response.getStatusCode() != HttpStatus.OK){
            log.error("Failed to share attachment files with users");
            if(response.hasBody() && response.getBody() != null){
                log.error("Integration request response: " + response.getBody());
            }
            throw new ResponseStatusException(response.getStatusCode(), "Failed to share attachment files");
        }
    }

    private void shareAvatarIfNotPublic(List<String> userIds, GroupChat chat, String avatarId, FileRecordDto avatarFileRecord) {
        if(!avatarFileRecord.getAccess().equals("PUBLIC")){
            if(!avatarFileRecord.getOwner().equals(chat.getAdminId())){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Could not share avatar file with users");
            }
            try {
                shareFiles(
                        Arrays.asList(avatarId),
                        chat.getAdminId(),
                        userIds
                );
            } catch (Exception ex){
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to share avatar file with users");
            }
        }
    }

    @Transactional
    private void addUsersToGroupChat(GroupChat chat, List<String> addedUserIds) {
        ResponseEntity<UserDto> response;
        List<ChatUser> verifiedUsers = new ArrayList<>();
        for (var userId: addedUserIds) {
            response = requestHandler.integrationRequest(
                    "users",
                    HttpMethod.GET,
                    UserDto.class,
                    String.format("/%s/exists", userId),
                    null
            );

            if(response.getStatusCode() != HttpStatus.OK || response.getBody() == null){
                if(response.getStatusCode() == HttpStatus.NOT_FOUND){
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Failed to add users to chat: user {id:%s} does not exist", userId));
                }
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to verify added user ids");
            }
            var user = response.getBody();
            verifiedUsers.add(
                new ChatUser(UUID.randomUUID().toString(),
                    userId,
                    chat,
                    false,
                    user.getAvatar(),
                    user.getFullName())
            );
        }

        chatUserRepository.saveAll(verifiedUsers);
        chat.getUsers().addAll(verifiedUsers);
    }

}
