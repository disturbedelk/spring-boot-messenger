package ru.tsu.hits.messenger.chat.entity;

public enum ChatType {
    DIALOGUE,
    GROUP_CHAT
}
