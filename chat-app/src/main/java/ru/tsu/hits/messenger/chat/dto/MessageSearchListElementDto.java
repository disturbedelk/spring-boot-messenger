package ru.tsu.hits.messenger.chat.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageSearchListElementDto {
    private String id;
    private String name;
    private String text;
    private Boolean containsAttachments;
    private Date sent;
    private List<String> attachmentNames;
}
