package ru.tsu.hits.messenger.chat.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroupMessageDto extends CreateUpdateMessageDto{
//    @NotBlank()
    private String targetChatId;
}
