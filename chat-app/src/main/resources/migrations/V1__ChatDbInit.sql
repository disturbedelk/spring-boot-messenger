--
-- PostgreSQL database dump
--

-- Dumped from database version 14.6
-- Dumped by pg_dump version 15.1 (Debian 15.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: attachment; Type: TABLE; Schema: public; Owner: chat-service
--

CREATE TABLE public.attachment (
    id character varying(255) NOT NULL,
    file_record_id character varying(255),
    name character varying(255),
    message_id character varying(255)
);


ALTER TABLE public.attachment OWNER TO "chat-service";

--
-- Name: chat; Type: TABLE; Schema: public; Owner: chat-service
--

CREATE TABLE public.chat (
    type character varying(31) NOT NULL,
    id character varying(255) NOT NULL,
    administrator_id character varying(255),
    chat_avatar_id character varying(255),
    date_created date,
    chat_name character varying(255)
);


ALTER TABLE public.chat OWNER TO "chat-service";

--
-- Name: chats_users; Type: TABLE; Schema: public; Owner: chat-service
--

CREATE TABLE public.chats_users (
    chat_id character varying(255) NOT NULL,
    user_id character varying(255) NOT NULL
);


ALTER TABLE public.chats_users OWNER TO "chat-service";

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: chat-service
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO "chat-service";

--
-- Name: message; Type: TABLE; Schema: public; Owner: chat-service
--

CREATE TABLE public.message (
    id character varying(255) NOT NULL,
    sent timestamp without time zone,
    message_content oid,
    chat_id character varying(255)
);


ALTER TABLE public.message OWNER TO "chat-service";

--
-- Data for Name: attachment; Type: TABLE DATA; Schema: public; Owner: chat-service
--

COPY public.attachment (id, file_record_id, name, message_id) FROM stdin;
\.


--
-- Data for Name: chat; Type: TABLE DATA; Schema: public; Owner: chat-service
--

COPY public.chat (type, id, administrator_id, chat_avatar_id, date_created, chat_name) FROM stdin;
\.


--
-- Data for Name: chats_users; Type: TABLE DATA; Schema: public; Owner: chat-service
--

COPY public.chats_users (chat_id, user_id) FROM stdin;
\.


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: chat-service
--

COPY public.message (id, sent, message_content, chat_id) FROM stdin;
\.


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: chat-service
--

SELECT pg_catalog.setval('public.hibernate_sequence', 1, false);


--
-- Name: attachment attachment_pkey; Type: CONSTRAINT; Schema: public; Owner: chat-service
--

ALTER TABLE ONLY public.attachment
    ADD CONSTRAINT attachment_pkey PRIMARY KEY (id);


--
-- Name: chat chat_pkey; Type: CONSTRAINT; Schema: public; Owner: chat-service
--

ALTER TABLE ONLY public.chat
    ADD CONSTRAINT chat_pkey PRIMARY KEY (id);


--
-- Name: chats_users chats_users_pkey; Type: CONSTRAINT; Schema: public; Owner: chat-service
--

ALTER TABLE ONLY public.chats_users
    ADD CONSTRAINT chats_users_pkey PRIMARY KEY (chat_id, user_id);


--
-- Name: message message_pkey; Type: CONSTRAINT; Schema: public; Owner: chat-service
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: message fkmejd0ykokrbuekwwgd5a5xt8a; Type: FK CONSTRAINT; Schema: public; Owner: chat-service
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT fkmejd0ykokrbuekwwgd5a5xt8a FOREIGN KEY (chat_id) REFERENCES public.chat(id);


--
-- Name: attachment fkoo11928qbsiolkc10dph1p214; Type: FK CONSTRAINT; Schema: public; Owner: chat-service
--

ALTER TABLE ONLY public.attachment
    ADD CONSTRAINT fkoo11928qbsiolkc10dph1p214 FOREIGN KEY (message_id) REFERENCES public.message(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

