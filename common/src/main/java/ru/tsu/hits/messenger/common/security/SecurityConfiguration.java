package ru.tsu.hits.messenger.common.security;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;
import ru.tsu.hits.messenger.common.security.auth.IntegrationFilter;
import ru.tsu.hits.messenger.common.security.auth.JwtFilter;
import ru.tsu.hits.messenger.common.security.props.SecurityProps;

import java.util.Arrays;
import java.util.Objects;

@Slf4j
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
@RequiredArgsConstructor
public class SecurityConfiguration {
    private final SecurityProps securityProps;
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @SneakyThrows
    @Bean
    public SecurityFilterChain filterChainJwt(HttpSecurity http){
        log.warn("Filter chain jwt running, adding filter");
        try {
            var jwtConfig = securityProps.getJwtToken();
            http = http.addFilterBefore(
                    new JwtFilter(jwtConfig.getSecret()),
                    UsernamePasswordAuthenticationFilter.class
            ).requestMatcher(filterPredicate(
                    jwtConfig.getRootPath(),
                    jwtConfig.getPermitAll(),
                    jwtConfig.getPermitExact()
            ));
        } catch(Exception ex){
            log.error(String.format("Failed building authentication filter chain: %s", ex.getMessage()));
        }
        return finalize(http);
    }

    @SneakyThrows
    @Bean
    public SecurityFilterChain filterChainIntegration(HttpSecurity http) {
        log.warn("Integration filter chain constructing");
        http = http.requestMatcher(filterPredicate(securityProps.getIntegrations().getRootPath()))
            .addFilterBefore(
                new IntegrationFilter(securityProps.getIntegrations().getApiKey()),
                UsernamePasswordAuthenticationFilter.class
            );
        return finalize(http);
    }

    @SneakyThrows
    private SecurityFilterChain finalize(HttpSecurity http) {
        return http.csrf()
            .disable()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .build();
    }

    private RequestMatcher filterPredicate(String rootPath) {
        return rq -> Objects.nonNull(rq.getServletPath())
                && rq.getServletPath().startsWith(rootPath);
    }
    private RequestMatcher filterPredicate(String rootPath, String... ignore) {
        return rq -> Objects.nonNull(rq.getServletPath())
                && rq.getServletPath().startsWith(rootPath)
                && Arrays.stream(ignore).noneMatch(item -> rq.getServletPath().startsWith(item));
    }
    private RequestMatcher filterPredicate(String rootPath, String[] ignore, String[] ignoreExact) {
        return rq -> Objects.nonNull(rq.getServletPath())
            && rq.getServletPath().startsWith(rootPath)
            && Arrays.stream(ignore).noneMatch(item -> rq.getServletPath().startsWith(item))
            && Arrays.stream(ignoreExact).noneMatch(item -> rq.getServletPath().equals(item));
    }

}
