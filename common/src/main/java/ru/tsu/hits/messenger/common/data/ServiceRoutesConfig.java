package ru.tsu.hits.messenger.common.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties("app.integration-routing")
@Getter
@Setter
@ToString
public class ServiceRoutesConfig {
    private List<ServiceRoute> services;
    public ServiceRoute get(String name){
        return services.stream()
                .filter(service -> service.name.equals(name))
                .findFirst()
                .orElse(null);
    }
}
