package ru.tsu.hits.messenger.common.security.props;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SecurityJwtProps {
    private String[] permitAll;
    private String[] permitExact;
    private String secret;
    private Long expiration;
    private String rootPath;
}
