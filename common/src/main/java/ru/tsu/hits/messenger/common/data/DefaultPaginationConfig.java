package ru.tsu.hits.messenger.common.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("app.service-config.pagination")
@Getter
@Setter
@ToString
public class DefaultPaginationConfig {
    private int defaultSize;
    private int maxSize;
}
