package ru.tsu.hits.messenger.common.security.data;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import ru.tsu.hits.messenger.common.security.data.JwtUserData;

public class JwtAuthentication extends AbstractAuthenticationToken {
    public JwtAuthentication(JwtUserData userData) {
        super(null);
        this.setDetails(userData);
        setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return getDetails();
    }
}
