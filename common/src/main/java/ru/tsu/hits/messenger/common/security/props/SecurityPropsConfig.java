package ru.tsu.hits.messenger.common.security.props;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class SecurityPropsConfig {
}
