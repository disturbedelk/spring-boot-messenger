package ru.tsu.hits.messenger.common.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ServiceRoute {
    public String name;
    public String host;
    public String port;
    public String path;
}
