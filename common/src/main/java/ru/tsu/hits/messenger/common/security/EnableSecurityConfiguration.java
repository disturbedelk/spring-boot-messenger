package ru.tsu.hits.messenger.common.security;

import org.springframework.context.annotation.Import;
import ru.tsu.hits.messenger.common.security.SecurityConfiguration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(SecurityConfiguration.class)
public @interface EnableSecurityConfiguration {
}
