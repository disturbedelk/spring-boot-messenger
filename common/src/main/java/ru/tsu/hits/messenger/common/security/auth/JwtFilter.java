package ru.tsu.hits.messenger.common.security.auth;

import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.tsu.hits.messenger.common.security.data.JwtAuthentication;
import ru.tsu.hits.messenger.common.security.data.JwtUserData;
import ru.tsu.hits.messenger.common.security.exceptions.UnauthorizedException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

import static ru.tsu.hits.messenger.common.security.data.SecurityConst.HEADER_JWT;

@Slf4j
@RequiredArgsConstructor
public class JwtFilter extends OncePerRequestFilter {
    private final String signingKey;
    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain
    ) throws ServletException, IOException {
        log.info("jwt filter entry");
        var jwt = request.getHeader(HEADER_JWT);
        if (jwt == null) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return;
        }
        if(jwt.startsWith("Bearer") || jwt.startsWith("bearer")){
            jwt = jwt.replaceFirst("(?i)\\bbearer\\b\\s", "");
        }

        log.info("JwtFilter processing");

        JwtUserData userData;
        try {
            var key = Keys.hmacShaKeyFor(signingKey.getBytes(StandardCharsets.UTF_8));
            var data = Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(jwt);
            var idStr = String.valueOf(data.getBody().get("id"));
            userData = new JwtUserData(
                idStr == null ? null : UUID.fromString(idStr),
                String.valueOf(data.getBody().get("login")),
                String.valueOf(data.getBody().get("name"))
            );
        } catch (JwtException e) {
//            throw new UnauthorizedException(e.getMessage());
            log.info(
                    String.format("Request jwt authentication failed: %s %s %s",
                    request.getMethod(),
                    request.getServletPath(),
                    e.getMessage())
            );
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return;
        }

        var authentication = new JwtAuthentication(userData);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(request, response);
    }
}
