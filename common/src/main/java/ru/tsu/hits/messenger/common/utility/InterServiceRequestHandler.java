package ru.tsu.hits.messenger.common.utility;

import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import ru.tsu.hits.messenger.common.data.ServiceRoutesConfig;
import ru.tsu.hits.messenger.common.security.data.SecurityConst;
import ru.tsu.hits.messenger.common.security.props.SecurityProps;

import java.net.ConnectException;
import java.util.Collections;

@Service
@RequiredArgsConstructor
@Slf4j
public class InterServiceRequestHandler {
    private final SecurityProps securityProps;
    private final ServiceRoutesConfig services;

    public ResponseEntity integrationRequest(String serviceName, HttpMethod httpMethod, Class responseClass, String path, Object body){
        log.info(String.format("Request handler integration request called for %s[%s %s]", serviceName, httpMethod.name(), path));
        var service = services.get(serviceName);
        if(service == null){
            throw new IllegalArgumentException(String.format("Service with name '%s' does not exist", serviceName));
        }

        var headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        headers.set(SecurityConst.HEADER_API_KEY, securityProps.getIntegrations().getApiKey());
        var requestPath = service.getHost() + ":" + service.getPort() + "/integration" + service.getPath() + path;
        HttpEntity request;
        if(body != null){
            request = new HttpEntity(body, headers);
        } else{
            request = new HttpEntity(headers);
        }
        ResponseEntity response;
        try {
            response = (new RestTemplate())
                .exchange(
                    requestPath,
                    httpMethod,
                    request,
                    responseClass
            );
        } catch (ResourceAccessException ex){
            log.info(ex.getMessage());
            throw new ResponseStatusException(HttpStatus.REQUEST_TIMEOUT, "Was unable to process request, try another time");
        }
        catch(HttpClientErrorException ex){
            log.info(ex.getMessage());
            response = new ResponseEntity(ex.getResponseBodyAsString(), ex.getStatusCode());
        }

//        if(response.getStatusCode() == HttpStatus.NOT_FOUND){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
//        }

        return response;
    }
}
