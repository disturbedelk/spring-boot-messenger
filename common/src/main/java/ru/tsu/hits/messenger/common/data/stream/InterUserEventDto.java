package ru.tsu.hits.messenger.common.data.stream;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InterUserEventDto {
    private String targetUserId;
    private String userId;
    private String userName;
}
