package ru.tsu.hits.messenger.common.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Pagination {
    private Integer page;
    private Integer size;
    private Integer total = null;
    public Pageable toPageable(){
        return PageRequest.of(page, size);
    }
    public static Pageable toPageable(Pagination pagination){ return pagination.toPageable(); }
    public static Pagination fromPageable(Pageable pageable) {
        return new Pagination(pageable.getPageNumber(), pageable.getPageSize(),null);
    }

    public void normalize(DefaultPaginationConfig config){
        if(size == null){
            size = config.getDefaultSize();
        }

        size = Math.max(Math.min(size, config.getMaxSize()),0);//clamp
        page = Math.max(page, 0);
        if(total != null){
            page = Math.min(page, total);
        }
    }

    public void normalize(int maxSize){
        if(size == null){
            size = 1;
        }

        size = Math.max(Math.min(size, maxSize),0);//clamp
        page = Math.max(page, 0);
        if(total != null){
            page = Math.min(page, total);
        }
    }
}
