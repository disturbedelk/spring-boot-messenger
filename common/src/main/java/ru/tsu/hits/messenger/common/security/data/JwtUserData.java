package ru.tsu.hits.messenger.common.security.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@Getter
@AllArgsConstructor
public class JwtUserData {
    private final UUID id;
    private final String login;
    private final String name;

}