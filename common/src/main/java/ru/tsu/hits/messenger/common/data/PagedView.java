package ru.tsu.hits.messenger.common.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PagedView<T>{
    List<T> content;
    Pagination pageInfo;
}
