ALTER TABLE ONLY public.user_relation
    DROP CONSTRAINT user_relation_pkey;
ALTER TABLE ONLY public.user_relation
    ADD CONSTRAINT user_relation_pkey PRIMARY KEY (target_user_uuid, useruuid, dtype);