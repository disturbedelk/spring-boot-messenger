--
-- PostgreSQL database dump
--

-- Dumped from database version 14.6
-- Dumped by pg_dump version 15.1 (Debian 15.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: user_relation; Type: TABLE; Schema: public; Owner: friends-service
--

CREATE TABLE public.user_relation (
    dtype character varying(31) NOT NULL,
    target_user_uuid character varying(255) NOT NULL,
    useruuid character varying(255) NOT NULL,
    created date,
    deleted date,
    is_valid boolean DEFAULT true,
    user_full_name character varying(255)
);


ALTER TABLE public.user_relation OWNER TO "friends-service";

--
-- Data for Name: user_relation; Type: TABLE DATA; Schema: public; Owner: friends-service
--

COPY public.user_relation (dtype, target_user_uuid, useruuid, created, deleted, is_valid, user_full_name) FROM stdin;
\.


--
-- Name: user_relation user_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: friends-service
--

ALTER TABLE ONLY public.user_relation
    ADD CONSTRAINT user_relation_pkey PRIMARY KEY (target_user_uuid, useruuid);


--
-- PostgreSQL database dump complete
--

