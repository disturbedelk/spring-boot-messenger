CREATE TABLE public.friends (
    target_user_uuid character varying(255) NOT NULL,
    useruuid character varying(255) NOT NULL,
    created date,
    deleted date,
    is_valid boolean DEFAULT true,
    user_full_name character varying(255)
);

ALTER TABLE public.friends OWNER TO "friends-service";

ALTER TABLE ONLY public.friends
    ADD CONSTRAINT friends_relation_pkey PRIMARY KEY (target_user_uuid, useruuid);

INSERT INTO public.friends (target_user_uuid, useruuid, created, deleted, is_valid, user_full_name)
SELECT target_user_uuid, useruuid, created, deleted, is_valid, user_full_name
FROM public.user_relation
WHERE dtype = 'FriendsRelation';

CREATE TABLE public.blacklist (
    target_user_uuid character varying(255) NOT NULL,
    useruuid character varying(255) NOT NULL,
    created date,
    deleted date,
    is_valid boolean DEFAULT true,
    user_full_name character varying(255)
);

ALTER TABLE public.blacklist OWNER TO "friends-service";

ALTER TABLE ONLY public.blacklist
    ADD CONSTRAINT blacklist_relation_pkey PRIMARY KEY (target_user_uuid, useruuid);

INSERT INTO public.blacklist (target_user_uuid, useruuid, created, deleted, is_valid, user_full_name)
SELECT target_user_uuid, useruuid, created, deleted, is_valid, user_full_name
FROM public.user_relation
WHERE dtype = 'BlacklistRelation';

ALTER TABLE ONLY public.user_relation
    RENAME TO user_relations_old;