package ru.tsu.hits.messenger.friends.utility;

import lombok.experimental.UtilityClass;
import org.hibernate.cfg.NotYetImplementedException;
import ru.tsu.hits.messenger.friends.dto.BlacklistedUserDto;
import ru.tsu.hits.messenger.friends.dto.FriendUserDto;
import ru.tsu.hits.messenger.friends.dto.UserRelationDto;
import ru.tsu.hits.messenger.friends.entity.BlacklistRelation;
import ru.tsu.hits.messenger.friends.entity.UserRelation;

public class BlacklistRelationEntityConverter implements IGenericRelationEntityConverter<BlacklistRelation, BlacklistedUserDto> {
    @Override
    public BlacklistedUserDto dtoFromEntity(BlacklistRelation rel){
        return dtoFromEntityBase(rel);
    }
    public BlacklistedUserDto dtoFromEntityBase(UserRelation rel){
        var rValue = new BlacklistedUserDto();

        rValue.setUserId(rel.getUserUUID());
        rValue.setFullName(rel.getUserFullName());
        rValue.setAdded(rel.getCreated());
        rValue.setDeleted(rel.getDeleted());
        rValue.setValid(rel.isValid());
        return rValue;
    }
    @Override
    public BlacklistRelation entityFromDto(BlacklistedUserDto dto){
        return entityFromDtoBase(dto);
    }

    public BlacklistRelation entityFromDtoBase(UserRelationDto dto){
        throw new NotYetImplementedException();
    }

    public BlacklistRelation createEntity(){
        return new BlacklistRelation();
    }

    public BlacklistedUserDto createDto(){
        return new BlacklistedUserDto();
    }
}
