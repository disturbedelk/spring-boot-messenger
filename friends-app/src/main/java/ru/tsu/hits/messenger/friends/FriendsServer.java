package ru.tsu.hits.messenger.friends;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import ru.tsu.hits.messenger.common.data.EnableDefaultPaginationConfiguration;
import ru.tsu.hits.messenger.common.data.EnableServiceRoutesConfig;
import ru.tsu.hits.messenger.common.security.EnableSecurityConfiguration;
import ru.tsu.hits.messenger.common.utility.EnableInterServiceRequestHandler;
import ru.tsu.hits.messenger.friends.utility.EnableEntityConverters;


@ConfigurationPropertiesScan("ru.tsu.hits.messenger.common.security")
@SpringBootApplication
@EnableSecurityConfiguration
@EnableServiceRoutesConfig
@EnableInterServiceRequestHandler
@EnableEntityConverters
@EnableDefaultPaginationConfiguration
public class FriendsServer {
	public static void main(String[] args) {
		SpringApplication.run(FriendsServer.class, args);
	}

}
