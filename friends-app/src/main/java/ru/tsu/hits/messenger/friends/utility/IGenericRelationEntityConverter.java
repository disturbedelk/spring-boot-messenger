package ru.tsu.hits.messenger.friends.utility;

import ru.tsu.hits.messenger.friends.dto.UserRelationDto;
import ru.tsu.hits.messenger.friends.entity.UserRelation;

public interface IGenericRelationEntityConverter<ent extends UserRelation, dto extends UserRelationDto> {
    ent entityFromDto(dto dto);

    dto dtoFromEntity(ent rel);

    ent createEntity();

    dto createDto();
}
