package ru.tsu.hits.messenger.friends.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import ru.tsu.hits.messenger.common.data.PagedView;
import ru.tsu.hits.messenger.common.data.Pagination;
import ru.tsu.hits.messenger.common.utility.InterServiceRequestHandler;
import ru.tsu.hits.messenger.friends.dto.BlacklistedUserDto;
import ru.tsu.hits.messenger.friends.dto.FriendUserDto;
import ru.tsu.hits.messenger.friends.entity.BlacklistRelation;
import ru.tsu.hits.messenger.friends.entity.FriendsRelation;
import ru.tsu.hits.messenger.friends.entity.RelationId;
import ru.tsu.hits.messenger.friends.repository.BlacklistRepository;
import ru.tsu.hits.messenger.friends.repository.FriendsRepository;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Deprecated
public class FriendsServiceLegacy {
    private final FriendsRepository friendsRepository;
    private final BlacklistRepository blacklistRepository;
    private final Logger logger = LoggerFactory.getLogger(FriendsServiceLegacy.class);
    private final InterServiceRequestHandler requestHandler;

    public PagedView<FriendUserDto> getUserFriends(String targetUserUUID, Pagination pagination, String fullName){
        var queryResult = friendsRepository.findUserRelationsWithFullNameWildcardPageable(
                targetUserUUID,
                fullName,
                pagination.toPageable()
        );
        pagination = Pagination.fromPageable(queryResult.getPageable());
        pagination.setTotal(queryResult.getTotalPages());

        var friends = (List<FriendsRelation>) queryResult.getContent();
        return new PagedView<FriendUserDto>(
            friends.stream()
                .map(FriendUserDto::new)
                .collect(Collectors.toList()),
            pagination
        );
    }

    public FriendUserDto getUserFriend(String targetUserUUID, String userUUID){
        var relation = getFriendRelation(targetUserUUID, userUUID);
        if(relation.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return new FriendUserDto(relation.get());
    }

    @Transactional
    public FriendUserDto addFriend(String targetUserUUID, String userUUID) throws ResponseStatusException {
        FriendsRelation friend = null;
        var existingFriendRelation = getFriendRelation(targetUserUUID, userUUID);
        if(existingFriendRelation.isPresent()){
            if(existingFriendRelation.get().isValid()) {
                logger.warn(String.format("Failed to add %s as friend to %s: relation already exists", userUUID, targetUserUUID));
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User friend relation already exists between " + targetUserUUID + " and " + userUUID);
            }
            friend = existingFriendRelation.get();
            friend.setDeleted(null);
        }

        var existingBlockedRelation = getBlacklistRelation(targetUserUUID, userUUID);
        if(existingBlockedRelation.isPresent() && existingBlockedRelation.get().isValid()){
            logger.warn(String.format("Failed to add %s as friend to %s: user is blacklisted", userUUID, targetUserUUID));
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Cannot befriend a blacklisted user, remove %s from  blacklist first", userUUID));
        }

        var existingBlockedByRelation = getBlacklistRelation(userUUID, targetUserUUID);
        if(existingBlockedByRelation.isPresent() && existingBlockedByRelation.get().isValid()){
            logger.warn(String.format("Failed to add %s as friend to %s: target user is blacklisted", userUUID, targetUserUUID));
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot befriend a user that has you blacklisted");
        }

        ResponseEntity response = getUserExt(userUUID);
        if(response.getBody() == null || response.getStatusCode() == HttpStatus.NOT_FOUND) {
            logger.error(String.format("Attempted to add user %s as friend to %s, operation failed: user not found", userUUID, targetUserUUID));
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Requested user was not found");
        }
        if(response.getStatusCode() != HttpStatus.OK){
            throw new ResponseStatusException(response.getStatusCode());
        }

        if(friend == null) {
            friend = new FriendsRelation();
        }
        friend.setTargetUserUUID(targetUserUUID);
        friend.setUserUUID(userUUID);

        var fullName = parseUserJsonProperty("fullName", response.getBody().toString());
        friend.setUserFullName(fullName);
        friend.setCreated(Date.from(Instant.now()));
        friend.setDeleted(null);
        friend.setValid(true);
        var saved = friendsRepository.save(friend);
        return new FriendUserDto(saved);
    }

    @Transactional
    public void syncUserData(String userId) {
        ResponseEntity response = getUserExt(userId);
        if(response.getBody() == null || response.getStatusCode() == HttpStatus.NOT_FOUND) {
            logger.error("Synchronization attempt failed for user " + userId + ": user record not found externally");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Requested user was not found");
        }
        if(response.getStatusCode() != HttpStatus.OK){
            throw new ResponseStatusException(response.getStatusCode());
        }

        var fullName = parseUserJsonProperty("fullName", response.getBody().toString());

        List<FriendsRelation> userFriendRelations = friendsRepository.findAllUserRelationsTargetOf(userId);
        for (var rel: userFriendRelations) {
            rel.setUserFullName(fullName);
        }
        friendsRepository.saveAll(userFriendRelations);
    }

    @Transactional
    public FriendUserDto removeFriend(String targetUserUUID, String userId) {
        var relation = getFriendRelation(targetUserUUID, userId);
        if(relation.isEmpty()){
            logger.error("Attempted friend removal of " + userId + " by " + targetUserUUID + " failed: relation does not exist");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        var friend = relation.get();
        friend.setDeleted(Date.from(Instant.now()));
        friend.setValid(false);
        var saved = friendsRepository.save(friend);
        return new FriendUserDto(saved);
    }


    public PagedView<BlacklistedUserDto> getUserBlacklist(String targetUserUUID, Pagination pagination, String fullName) {
        var queryResult = friendsRepository.findUserRelationsWithFullNameWildcardPageable(
                targetUserUUID,
                fullName,
                pagination.toPageable()
        );
        pagination = Pagination.fromPageable(queryResult.getPageable());
        pagination.setTotal(queryResult.getTotalPages());

        var blacklist = (List<FriendsRelation>) queryResult.getContent();
        return new PagedView<BlacklistedUserDto>(
                blacklist.stream()
                        .map(BlacklistedUserDto::new)
                        .collect(Collectors.toList()),
                pagination
        );
    }



    private Optional<FriendsRelation> getFriendRelation(String targetUserUUID, String userUUID){
        return friendsRepository.findById(new RelationId(targetUserUUID, userUUID));
    }

    public Optional<BlacklistRelation> getBlacklistRelation(String targetUserUUID, String userUUID){
        return blacklistRepository.findById(new RelationId(targetUserUUID, userUUID));
    }

    public BlacklistedUserDto addBlacklisted(String targetUserUUID, String userUUID) {
        BlacklistRelation blacklistedUser = null;
        var existingBlacklistRelation = getBlacklistRelation(targetUserUUID, userUUID);
        if(existingBlacklistRelation.isPresent()){
            if(existingBlacklistRelation.get().isValid()) {
                logger.warn(String.format("Failed to add %s as blacklisted to %s: relation already exists", userUUID, targetUserUUID));
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User blacklist relation already exists between " + targetUserUUID + " and " + userUUID);
            }
            blacklistedUser = (BlacklistRelation) existingBlacklistRelation.get();
            blacklistedUser.setDeleted(null);
        }

        ResponseEntity response = getUserExt(userUUID);
        if(response.getBody() == null || response.getStatusCode() == HttpStatus.NOT_FOUND) {
            logger.error(String.format("Attempted to add user %s as blacklisted to %s, operation failed: user not found", userUUID, targetUserUUID));
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Requested user was not found");
        }
        if(response.getStatusCode() != HttpStatus.OK){
            throw new ResponseStatusException(response.getStatusCode());
        }

        if(blacklistedUser == null) {
            blacklistedUser = new BlacklistRelation();
        }
        blacklistedUser.setTargetUserUUID(targetUserUUID);
        blacklistedUser.setUserUUID(userUUID);

        var fullName = parseUserJsonProperty("fullName", response.getBody().toString());
        blacklistedUser.setUserFullName(fullName);
        blacklistedUser.setDeleted(null);
        blacklistedUser.setValid(true);
        var saved = blacklistRepository.save(blacklistedUser);
        return new BlacklistedUserDto(saved);
    }

    public BlacklistedUserDto removeBlacklisted(String targetUserUUID, String userId) {
        var relation = getBlacklistRelation(targetUserUUID, userId);
        if(relation.isEmpty()){
            logger.error("Attempted blacklist removal of " + userId + " from " + targetUserUUID + " failed: relation does not exist");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        var blacklisted = relation.get();
        blacklisted.setDeleted(Date.from(Instant.now()));
        blacklisted.setValid(false);
        var saved = blacklistRepository.save(blacklisted);
        return new BlacklistedUserDto(saved);
    }

    public BlacklistedUserDto getBlacklistedOfUser(String targetUserUUID, String userUUID) {
        var relation = getBlacklistRelation(targetUserUUID, userUUID);
        if(relation.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return new BlacklistedUserDto(relation.get());
    }

    private ResponseEntity getUserExt(String userUUID){
        return requestHandler.integrationRequest(
                "users",
                HttpMethod.GET,
                String.class,
                String.format("/%s/exists", userUUID),
                null
        );
    }

    private String parseUserJsonProperty(String property, String jsonString){
        String rValue;
        try{
            ObjectMapper mapper = new ObjectMapper();
            JsonNode tree = mapper.readTree(jsonString);
            JsonNode node = tree.get(property);
            rValue = node.textValue();
        }
        catch (Exception ex){
            rValue = "";
        }
        return rValue;
    }
}
