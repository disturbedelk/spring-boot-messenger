package ru.tsu.hits.messenger.friends.dto;

import ru.tsu.hits.messenger.friends.entity.UserRelation;

public class BlacklistedUserDto extends UserRelationDto{
    public BlacklistedUserDto(){
        super();
    }
    public BlacklistedUserDto(UserRelation rel){
        super(rel);
    }
}
