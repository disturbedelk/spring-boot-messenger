package ru.tsu.hits.messenger.friends.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.tsu.hits.messenger.common.data.DefaultPaginationConfig;
import ru.tsu.hits.messenger.common.utility.InterServiceRequestHandler;
import ru.tsu.hits.messenger.friends.dto.BlacklistedUserDto;
import ru.tsu.hits.messenger.friends.dto.FriendUserDto;
import ru.tsu.hits.messenger.friends.entity.BlacklistRelation;
import ru.tsu.hits.messenger.friends.entity.FriendsRelation;
import ru.tsu.hits.messenger.friends.repository.RelationsRepository;
import ru.tsu.hits.messenger.friends.utility.IGenericRelationEntityConverter;

@Slf4j
@Service
public class BlacklistService extends RelationsService<BlacklistRelation, BlacklistedUserDto> {
    private final InterServiceRequestHandler requestHandler;
    public BlacklistService(
            RelationsRepository<BlacklistRelation> repository,
            InterServiceRequestHandler requestHandler,
            IGenericRelationEntityConverter<BlacklistRelation, BlacklistedUserDto> entityConverter,
            DefaultPaginationConfig paginationConfig
    ){
        super(repository, requestHandler, entityConverter, paginationConfig);
        this.requestHandler = requestHandler;
    }

    @Override
    public BlacklistedUserDto addUserRelation(String targetUserUUID, String userUUID) throws ResponseStatusException {
        var response = requestHandler.integrationRequest(
                "friends",
                HttpMethod.GET,
                String.class,
                String.format("/exists?targetUserId=%s&userId=%s", targetUserUUID, userUUID),
                ""
        );
        if(response.getStatusCode() != HttpStatus.OK){
            log.warn(String.format("Blacklist service failed to retrieve friends data for %s", targetUserUUID));
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to retrieve user data");
        }

        if(response.getStatusCode() == HttpStatus.OK && response.getBody().toString().contains("true")){
            response = requestHandler.integrationRequest(
                    "friends",
                    HttpMethod.POST,
                    String.class,
                    String.format("/remove?targetUserId=%s&userId=%s", targetUserUUID, userUUID),
                    ""
            );
            if(response.getStatusCode() != HttpStatus.OK && response.getStatusCode() != HttpStatus.NOT_FOUND){
                log.warn(String.format("Blacklist service failed to perform integration request to remove %s from friends list of %s; [%s] %s",
                        userUUID,
                        targetUserUUID,
                        response.getStatusCode(),
                        response
                ));
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to perform user relation operation");
            }
        }

        return super.addUserRelation(targetUserUUID, userUUID);
    }
}
