package ru.tsu.hits.messenger.friends.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.tsu.hits.messenger.common.data.DefaultPaginationConfig;
import ru.tsu.hits.messenger.common.utility.InterServiceRequestHandler;
import ru.tsu.hits.messenger.friends.dto.FriendUserDto;
import ru.tsu.hits.messenger.friends.entity.FriendsRelation;
import ru.tsu.hits.messenger.friends.repository.RelationsRepository;
import ru.tsu.hits.messenger.friends.utility.IGenericRelationEntityConverter;

@Service
@Slf4j
public class FriendsService extends RelationsService<FriendsRelation, FriendUserDto> {
    private final InterServiceRequestHandler requestHandler;
    public FriendsService(
            RelationsRepository<FriendsRelation> repository,
            InterServiceRequestHandler requestHandler,
            IGenericRelationEntityConverter<FriendsRelation, FriendUserDto> entityConverter,
            DefaultPaginationConfig paginationConfig
    ){
        super(repository, requestHandler, entityConverter, paginationConfig);
        this.requestHandler = requestHandler;
    }

    //Add blacklist checks and pass to super
    @Override
    public FriendUserDto addUserRelation(String targetUserUUID, String userUUID) throws ResponseStatusException {
        var response = requestHandler.integrationRequest(
                "friends",
                HttpMethod.GET,
                String.class,
                String.format("/blacklist/exists?targetUserId=%s&userId=%s", userUUID, targetUserUUID),
                ""
        );
        if(response.getStatusCode() == HttpStatus.OK && response.getBody().toString().contains("true")){
            log.warn(response.getBody().toString());
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You are blacklisted by this user");
        }
        if(response.getStatusCode() != HttpStatus.OK){
            log.warn(String.format("Friends service failed to retrieve blacklist data for %s", userUUID));
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to retrieve user data");
        }

        response = requestHandler.integrationRequest(
                "friends",
                HttpMethod.GET,
                String.class,
                String.format("/blacklist/exists?targetUserId=%s&userId=%s", targetUserUUID, userUUID),
                ""
        );
        if(response.getStatusCode() == HttpStatus.OK && response.getBody().toString().contains("true")){
            log.warn(response.getBody().toString());
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You have blacklisted this user, remove from blacklist to add as friend");
        }
        if(response.getStatusCode() != HttpStatus.OK){
            log.warn(String.format("Friends service failed to retrieve blacklist data for %s", targetUserUUID));
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to retrieve user data");
        }

        return super.addUserRelation(targetUserUUID, userUUID);
    }
}
