package ru.tsu.hits.messenger.friends.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RelationId implements Serializable {
    private String targetUserUUID;
    private String userUUID;
}
