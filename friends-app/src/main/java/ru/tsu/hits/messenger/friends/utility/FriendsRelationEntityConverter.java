package ru.tsu.hits.messenger.friends.utility;

import lombok.experimental.UtilityClass;
import org.hibernate.cfg.NotYetImplementedException;
import ru.tsu.hits.messenger.friends.dto.FriendUserDto;
import ru.tsu.hits.messenger.friends.dto.UserRelationDto;
import ru.tsu.hits.messenger.friends.entity.FriendsRelation;
import ru.tsu.hits.messenger.friends.entity.UserRelation;

public class FriendsRelationEntityConverter implements IGenericRelationEntityConverter<FriendsRelation, FriendUserDto> {
    @Override
    public FriendUserDto dtoFromEntity(FriendsRelation rel){
        return dtoFromEntityBase((UserRelation) rel);
    }
    public FriendUserDto dtoFromEntityBase(UserRelation rel){
        var rValue = new FriendUserDto();

        rValue.setUserId(rel.getUserUUID());
        rValue.setFullName(rel.getUserFullName());
        rValue.setAdded(rel.getCreated());
        rValue.setDeleted(rel.getDeleted());
        rValue.setValid(rel.isValid());
        return rValue;
    }
    @Override
    public FriendsRelation entityFromDto(FriendUserDto dto){
        return entityFromDtoBase((UserRelationDto) dto);
    }

    public FriendsRelation entityFromDtoBase(UserRelationDto dto){
        throw new NotYetImplementedException();
    }

    public FriendsRelation createEntity(){
        return new FriendsRelation();
    }

    public FriendUserDto createDto(){
        return new FriendUserDto();
    }
}
