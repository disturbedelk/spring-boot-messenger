package ru.tsu.hits.messenger.friends.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.tsu.hits.messenger.friends.service.BlacklistService;
import ru.tsu.hits.messenger.friends.service.FriendsService;

@RestController
@RequestMapping("/integration/friends")
@RequiredArgsConstructor
@Slf4j
public class FriendsIntegrationController {
    private final FriendsService friendsService;
    private final BlacklistService blacklistService;
    @PatchMapping("/{userId}/sync")
    @ResponseStatus(value = HttpStatus.OK)
    private void syncUserFriendData(@PathVariable String userId){
        friendsService.syncUserData(userId);
        blacklistService.syncUserData(userId);
    }
    @GetMapping("/blacklist/exists")
    private boolean blacklistRelationExists(
            @RequestParam(name = "targetUserId", required = true) String targetUserId,
            @RequestParam(name = "userId", required = true) String userId){
        return blacklistService.userRelationExistsAndValid(targetUserId, userId);
    }

    @GetMapping("/exists")
    private boolean friendsRelationExists(
            @RequestParam(name = "targetUserId", required = true) String targetUserId,
            @RequestParam(name = "userId", required = true) String userId){
        return friendsService.userRelationExistsAndValid(targetUserId, userId);
    }

    @PostMapping("/remove")
    private boolean removeFriendsRelation(
            @RequestParam(name = "targetUserId", required = true) String targetUserId,
            @RequestParam(name = "userId", required = true) String userId){
        try {
            friendsService.removeRelation(targetUserId, userId);
        } catch (ResponseStatusException ex){
            log.info(String.format("Attempted to remove friends relation between %s and %s, yielding exception: %s %s",
                    targetUserId,
                    userId,
                    ex.getStatus(),
                    ex.getMessage()));
            return false;
        }
        return true;
    }
}
