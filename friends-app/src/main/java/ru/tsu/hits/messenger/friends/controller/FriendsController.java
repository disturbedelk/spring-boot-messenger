package ru.tsu.hits.messenger.friends.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.tsu.hits.messenger.common.data.PagedView;
import ru.tsu.hits.messenger.common.security.data.JwtUserData;
import ru.tsu.hits.messenger.friends.dto.BlacklistedUserDto;
import ru.tsu.hits.messenger.friends.dto.FriendUserDto;
import ru.tsu.hits.messenger.friends.dto.GetRelationsPagedRequestBody;
import ru.tsu.hits.messenger.friends.service.BlacklistService;
import ru.tsu.hits.messenger.friends.service.FriendsService;

@RestController
@RequestMapping("/api/friends")
@RequiredArgsConstructor
public class FriendsController {
    private final FriendsService friendsService;
    private final BlacklistService blacklistService;
    @PostMapping
    private PagedView<FriendUserDto> getFriends(Authentication authentication, @RequestBody GetRelationsPagedRequestBody body){
        String targetUserId = ((JwtUserData)authentication.getPrincipal()).getId().toString();
        return friendsService.getUserRelations(
                targetUserId,
                body.getPagination(),
                body.getFullName()
        );
    }

    @GetMapping("/{userId}")
    private FriendUserDto getFriendInfo(Authentication authentication, @PathVariable String userId){
        return friendsService.getUserRelation(((JwtUserData)authentication.getPrincipal()).getId().toString(), userId);
    }

    @PostMapping("/{userId}/add")
    private FriendUserDto addFriend(Authentication authentication, @PathVariable(required = true) String userId){
        return friendsService.addUserRelation(((JwtUserData)authentication.getPrincipal()).getId().toString(), userId);
    }

    @PostMapping("/{userId}/remove")
    private FriendUserDto removeFriend(Authentication authentication, @PathVariable String userId){
        return  friendsService.removeRelation(((JwtUserData)authentication.getPrincipal()).getId().toString(), userId);
    }

    @PostMapping("/blacklist")
    private PagedView<BlacklistedUserDto> getBlacklisted(Authentication authentication, @RequestBody GetRelationsPagedRequestBody body){
        String targetUserId = ((JwtUserData)authentication.getPrincipal()).getId().toString();

        return blacklistService.getUserRelations(
                targetUserId,
                body.getPagination(),
                body.getFullName()
        );
    }

    @GetMapping("/blacklist/{userId}/info")
    private BlacklistedUserDto getBlacklistedInfo(Authentication authentication, @PathVariable String userId){
        String targetUserId = ((JwtUserData)authentication.getPrincipal()).getId().toString();
        return blacklistService.getUserRelation(targetUserId, userId);
    }

    @PostMapping("/blacklist/{userId}/add")
    private BlacklistedUserDto blacklistUser(Authentication authentication, @PathVariable String userId){
        String targetUserId = ((JwtUserData)authentication.getPrincipal()).getId().toString();
        return blacklistService.addUserRelation(targetUserId, userId);
    }

    @PostMapping("/blacklist/{userId}/remove")
    private BlacklistedUserDto removeFromBlacklist(Authentication authentication, @PathVariable String userId){
        String targetUserId = ((JwtUserData)authentication.getPrincipal()).getId().toString();
        return blacklistService.removeRelation(targetUserId, userId);
    }

    @GetMapping("/blacklist/{userId}/check")
    private boolean amInBlacklist(Authentication authentication, @PathVariable String userId){
        String targetUserId = ((JwtUserData)authentication.getPrincipal()).getId().toString();
        return blacklistService.userRelationExistsAndValid(targetUserId, userId);
    }
}
