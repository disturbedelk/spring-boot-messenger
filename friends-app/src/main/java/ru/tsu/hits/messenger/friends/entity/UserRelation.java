package ru.tsu.hits.messenger.friends.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Data
@NoArgsConstructor
@AllArgsConstructor
@IdClass(RelationId.class)
public class UserRelation {
    @Column(name = "target_user_uuid")
    @NonNull
    @Id
    private String targetUserUUID;
    @Column
    @NonNull
    @Id
    private String userUUID;

    @Column
    @CreatedDate
    @Temporal(TemporalType.DATE)
    private Date created;

    @Column(name = "deleted",  nullable = true)
    @Temporal(TemporalType.DATE)
    private Date deleted;
    @Column(name = "is_valid", columnDefinition = "boolean default true")
    private boolean isValid;
    @Column
    private String userFullName;
}
