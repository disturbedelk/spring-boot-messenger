package ru.tsu.hits.messenger.friends.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.tsu.hits.messenger.friends.entity.RelationId;


import java.util.List;

public interface RelationsRepository<UserRelation extends ru.tsu.hits.messenger.friends.entity.UserRelation>
        extends JpaRepository<UserRelation, RelationId>, JpaSpecificationExecutor<UserRelation> {
    List<UserRelation> findByTargetUserUUID(String targetUserUUID);
    List<UserRelation> findByTargetUserUUID(String targetUserUUID, Pageable pageable);
    List<UserRelation> findByUserUUID(String userUUID);
//    @Query("SELECT e FROM #{#entityName} e WHERE e.targetUserUUID = ?1 or e.userUUID = ?1")
//    List<UserRelation> findAllUserReltaions(String userUUID);
    List<UserRelation> findAll(Specification<UserRelation> spec);
    Page<UserRelation> findAll(Specification<UserRelation> spec, Pageable pageable);
    default Page<UserRelation> findUserRelationsWithFullNameWildcardPageable(String userUUID, String fullName, Pageable pageable){
        Specification<UserRelation> specification = (root, query, criteriaBuilder) ->
                criteriaBuilder.and(
                    criteriaBuilder.equal(root.get("targetUserUUID"), userUUID),
                    criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("userFullName")),
                        "%" + (fullName == null ? "" : fullName.toLowerCase()) + "%"
                    ),
                    criteriaBuilder.isNull(root.get("deleted")),
                    criteriaBuilder.isTrue(root.get("isValid"))
                );

        return findAll(specification, pageable);
    }

    default List<UserRelation> findAllUserRelationsTargetOf(String userUUID){
        Specification<UserRelation> specification = ((root, query, criteriaBuilder) ->
                criteriaBuilder.and(
                    criteriaBuilder.equal(root.get("targetUserUUID"), userUUID),
                    criteriaBuilder.isNull(root.get("deleted")),
                    criteriaBuilder.isTrue(root.get("isValid"))
                )
            );
        return findAll(specification);
    }
}
