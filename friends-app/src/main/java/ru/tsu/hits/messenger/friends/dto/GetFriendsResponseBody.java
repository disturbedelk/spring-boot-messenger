package ru.tsu.hits.messenger.friends.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetFriendsResponseBody {
    private List<FriendUserDto> friends;
    private Pageable pagination;
}
