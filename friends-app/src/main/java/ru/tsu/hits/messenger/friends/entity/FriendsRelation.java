package ru.tsu.hits.messenger.friends.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "friends")
@IdClass(RelationId.class)
public class FriendsRelation extends UserRelation{
}
