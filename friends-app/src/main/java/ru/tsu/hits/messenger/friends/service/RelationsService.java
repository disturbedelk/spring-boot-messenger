package ru.tsu.hits.messenger.friends.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import ru.tsu.hits.messenger.common.data.DefaultPaginationConfig;
import ru.tsu.hits.messenger.common.data.PagedView;
import ru.tsu.hits.messenger.common.data.Pagination;
import ru.tsu.hits.messenger.common.utility.InterServiceRequestHandler;
import ru.tsu.hits.messenger.friends.entity.RelationId;
import ru.tsu.hits.messenger.friends.repository.RelationsRepository;
import ru.tsu.hits.messenger.friends.utility.IGenericRelationEntityConverter;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
public class RelationsService<
            UserRelation extends ru.tsu.hits.messenger.friends.entity.UserRelation,
            UserRelationDto extends ru.tsu.hits.messenger.friends.dto.UserRelationDto
        > {
    private final RelationsRepository<UserRelation> relationsRepository;
    private final InterServiceRequestHandler requestHandler;
    private final IGenericRelationEntityConverter<UserRelation, UserRelationDto> converter;
    private final DefaultPaginationConfig paginationConfig;

    public PagedView<UserRelationDto> getUserRelations(String targetUserUUID, Pagination pagination, String fullName){
        pagination.setPage(pagination.getPage() - 1);
        pagination.normalize(paginationConfig);
        var queryResult = relationsRepository.findUserRelationsWithFullNameWildcardPageable(
            targetUserUUID,
            fullName,
            pagination.toPageable()
        );
        pagination = Pagination.fromPageable(queryResult.getPageable());
        pagination.setTotal(queryResult.getTotalPages());
        pagination.setPage(pagination.getPage() + 1);

        var relations = (List<UserRelation>) queryResult.getContent();
        return new PagedView<UserRelationDto>(
                relations.stream()
                        .map(converter::dtoFromEntity)
                        .collect(Collectors.toList()),
                pagination
        );
    }

    public UserRelationDto getUserRelation(String targetUserUUID, String userUUID){
        var relation = getRelation(targetUserUUID, userUUID);
        if(relation.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return converter.dtoFromEntity(relation.get());
    }

    public boolean userRelationExistsAndValid(String targetUserUUID, String userUUID){
        var relation = getRelation(targetUserUUID, userUUID);
        return relation.isPresent() && relation.get().isValid();
    }

    @Transactional
    public UserRelationDto addUserRelation(String targetUserUUID, String userUUID) throws ResponseStatusException {
        if(targetUserUUID.equals(userUUID)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Matching Target and external user ids");
        }

        UserRelation relation = null;
        var existingFriendRelation = getRelation(targetUserUUID, userUUID);
        if(existingFriendRelation.isPresent()){
            if(existingFriendRelation.get().isValid()) {
                log.warn(String.format("Failed to add relation of %s with %s: relation already exists", userUUID, targetUserUUID));
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("User friend relation already exists between %s and %s", targetUserUUID, userUUID));
            }
            relation = existingFriendRelation.get();
            relation.setDeleted(null);
        }

        ResponseEntity response = getUserExt(userUUID);
        if(response.getBody() == null || response.getStatusCode() == HttpStatus.NOT_FOUND) {
            log.error(String.format("[%s] Attempted to add relation between user %s and %s, operation failed: user not found", this.getClass().getName(), userUUID, targetUserUUID));
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Requested user was not found");
        }
        if(response.getStatusCode() != HttpStatus.OK){
            throw new ResponseStatusException(response.getStatusCode());
        }

        if(relation == null) {
            relation = converter.createEntity();
        }
        relation.setTargetUserUUID(targetUserUUID);
        relation.setUserUUID(userUUID);

        var fullName = parseUserJsonProperty("fullName", response.getBody().toString());
        relation.setUserFullName(fullName);
        relation.setCreated(Date.from(Instant.now()));
        relation.setDeleted(null);
        relation.setValid(true);
        var saved = relationsRepository.save(relation);
        return converter.dtoFromEntity(relation);
    }

    @Transactional
    public void syncUserData(String userId) {
        ResponseEntity response = getUserExt(userId);
        if(response.getBody() == null || response.getStatusCode() == HttpStatus.NOT_FOUND) {
            log.error("Synchronization attempt failed for user " + userId + ": user record not found externally");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Requested user was not found");
        }
        if(response.getStatusCode() != HttpStatus.OK){
            throw new ResponseStatusException(response.getStatusCode());
        }

        var fullName = parseUserJsonProperty("fullName", response.getBody().toString());

        List<UserRelation> userRelations = relationsRepository.findAllUserRelationsTargetOf(userId);
        for (var rel: userRelations) {
            rel.setUserFullName(fullName);
        }
        relationsRepository.saveAll(userRelations);
    }

    @Transactional
    public UserRelationDto removeRelation(String targetUserUUID, String userId) {
        var relation = getRelation(targetUserUUID, userId);
        if(relation.isEmpty()){
            log.error("Attempted modifying relation to removal of " + userId + " by " + targetUserUUID + " failed: relation does not exist");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        var friend = relation.get();
        friend.setDeleted(Date.from(Instant.now()));
        friend.setValid(false);
        var saved = relationsRepository.save(friend);
        return converter.dtoFromEntity(saved);
    }

    private Optional<UserRelation> getRelation(String targetUserUUID, String userUUID){
        return relationsRepository.findById(new RelationId(targetUserUUID, userUUID));
    }

    private ResponseEntity getUserExt(String userUUID){
        return requestHandler.integrationRequest(
                "users",
                HttpMethod.GET,
                String.class,
                String.format("/%s/exists", userUUID),
                null
        );
    }

    private String parseUserJsonProperty(String property, String jsonString){
        String rValue;
        try{
            ObjectMapper mapper = new ObjectMapper();
            JsonNode tree = mapper.readTree(jsonString);
            JsonNode node = tree.get(property);
            rValue = node.textValue();
        }
        catch (Exception ex){
            rValue = "";
        }
        return rValue;
    }
}
