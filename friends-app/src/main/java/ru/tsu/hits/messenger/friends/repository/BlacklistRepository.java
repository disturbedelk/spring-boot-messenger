package ru.tsu.hits.messenger.friends.repository;

import ru.tsu.hits.messenger.friends.entity.BlacklistRelation;

public interface BlacklistRepository extends RelationsRepository<BlacklistRelation> {
}
