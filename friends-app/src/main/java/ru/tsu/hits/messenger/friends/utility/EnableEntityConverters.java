package ru.tsu.hits.messenger.friends.utility;

import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({FriendsRelationEntityConverter.class, BlacklistRelationEntityConverter.class})
public @interface EnableEntityConverters {
}
