package ru.tsu.hits.messenger.friends.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.tsu.hits.messenger.friends.entity.UserRelation;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRelationDto {
    @NonNull
    private String userId;
    @NonNull
    private String fullName;
    private Date added;
    private Date deleted;
    private boolean valid;
    public UserRelationDto(UserRelation relation){
        userId = relation.getUserUUID();
        fullName = relation.getUserFullName();
        added = relation.getCreated();
        deleted = relation.getDeleted();
        valid = relation.isValid();
    }
}
