package ru.tsu.hits.messenger.friends.repository;

import ru.tsu.hits.messenger.friends.entity.FriendsRelation;

public interface FriendsRepository extends RelationsRepository<FriendsRelation>{
}
