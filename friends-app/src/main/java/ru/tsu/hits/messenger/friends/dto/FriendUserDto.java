package ru.tsu.hits.messenger.friends.dto;

import lombok.AllArgsConstructor;
import ru.tsu.hits.messenger.friends.entity.UserRelation;

public class FriendUserDto extends UserRelationDto{
    public FriendUserDto(){
        super();
    }
    public FriendUserDto(UserRelation rel){
        super(rel);
    }
}
