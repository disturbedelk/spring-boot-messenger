package ru.tsu.hits.messenger.friends.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "blacklist")
@IdClass(RelationId.class)
public class BlacklistRelation extends UserRelation{
}
