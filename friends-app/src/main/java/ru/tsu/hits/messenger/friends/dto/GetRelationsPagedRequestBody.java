package ru.tsu.hits.messenger.friends.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tsu.hits.messenger.common.data.Pagination;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetRelationsPagedRequestBody {
    private Pagination pagination;
    private String fullName = null;
}
